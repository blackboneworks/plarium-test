﻿using System.Linq;
using Controllers.Game.Units.Buffs;
using UnityEditor;
using UnityEngine;

namespace Inspectors.Components.Units
{
    [CustomEditor(typeof(BuffHub))]
    public class BuffHubInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            var target = this.target as BuffHub; ;

            EditorGUILayout.LabelField("Buff Hub");
            EditorGUILayout.LabelField("Buffs total: " + target.BuffCount);

            for (int i = 0; i < target.BuffCount; i++)
            {
                var buff = target.Buffs.ElementAt(i);
                GUILayout.BeginVertical("box");
                GUILayout.Label("Type: " + buff.GetType().Name);

                if (buff is ParameterModifierBuff)
                    DrawBuffAsParameterModifierBuff(buff as ParameterModifierBuff);

                GUILayout.EndVertical();
            }
        }

        private void DrawBuffAsParameterModifierBuff(ParameterModifierBuff buff)
        {
            GUILayout.BeginVertical("box");
            
            EditorGUILayout.LabelField("Parameters Changes", EditorStyles.boldLabel);
            foreach (var upgrade in buff.ParameterUpgrades)
            {
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(upgrade.Key.Name + " : ");
                EditorGUILayout.LabelField(upgrade.Value.ToString());
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }
    }
}