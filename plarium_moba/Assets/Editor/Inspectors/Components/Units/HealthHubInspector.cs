﻿using Controllers.Game.Units.HealthSystem;
using UnityEditor;
using UnityEngine;

namespace Inspectors.Components.Units
{
    [CustomEditor(typeof(HealthHub))]
    public class HealthHubInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            var target = this.target as HealthHub;

            var max = target.Owner != null ? target.Owner.BaseHealth : 0;

            ProgressBar(target.NormalizedHealth, target.Health + "/" + max);
        }
        
        void ProgressBar(float value, string label)
        {
            Rect rect = GUILayoutUtility.GetRect(18, 18, "TextField");
            
            EditorGUI.ProgressBar(rect, value, label);
            EditorGUILayout.Space();
        }
    }
}