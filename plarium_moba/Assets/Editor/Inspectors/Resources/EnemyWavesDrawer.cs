﻿using Models.Buildings;
using UnityEditor;
using UnityEngine;

namespace Inspectors.Resources
{
    [CustomPropertyDrawer(typeof(EnemiesWave))]
    public class EnemyWavesDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight * 2 + 3 + EditorGUI.GetPropertyHeight(property.FindPropertyRelative("Units"));
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.HelpBox(position, "", MessageType.None);
            
            var delayPosition = new Rect(position.x, position.y + 3, position.width - 3,
                EditorGUIUtility.singleLineHeight);
            var delayPerUnitPosition = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight + 3, position.width - 3,
                EditorGUIUtility.singleLineHeight);
            var unitsPosition = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 2,
                position.width - 3, EditorGUI.GetPropertyHeight(property.FindPropertyRelative("Units")));

            EditorGUI.PropertyField(delayPosition, property.FindPropertyRelative("Delay"));
            EditorGUI.PropertyField(delayPerUnitPosition, property.FindPropertyRelative("PerUnitDelay"));
            EditorGUI.PropertyField(unitsPosition, property.FindPropertyRelative("Units"), true);

        }

        [CustomPropertyDrawer(typeof(UnitAmount))]
        public class UnitAmountDrawer : PropertyDrawer
        {
            public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            {
                var unitPosition = new Rect(position.x, position.y, position.width / 2,
                    EditorGUIUtility.singleLineHeight);
                var amountPosition = new Rect(position.x + position.width / 2, position.y, position.width / 2,
                    EditorGUIUtility.singleLineHeight);

                EditorGUI.PropertyField(unitPosition, property.FindPropertyRelative("Unit"), new GUIContent());
                EditorGUI.PropertyField(amountPosition, property.FindPropertyRelative("Amount"), new GUIContent());
            }
        }
    }
}