﻿using System.Linq;
using Models.Parameters;
using UnityEditor;
using UnityEngine;

namespace Inspectors.Resources
{
    #region PARAMETER

    public abstract class ParameterDrawer<T> : PropertyDrawer where T : Parameter
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var parameters = ((T[]) typeof(T).GetMethod("GetAllParameters").Invoke(null, new object[0])).ToList();

            EditorGUI.BeginProperty(position, label, property);

            var index = EditorGUI.Popup(position, parameters.IndexOf((T)property.objectReferenceValue),
                parameters.Select(x => x.name).ToArray());

            if ((index < 0) || (index >= parameters.Count)) index = 0;

            if (parameters.Count > 0)
                property.objectReferenceValue = parameters[index];

            EditorGUI.EndProperty();
        }
    }

    [CustomPropertyDrawer(typeof(UnitParameter))]
    public class UnitParameterDrawer : ParameterDrawer<UnitParameter>
    {
    }

    [CustomPropertyDrawer(typeof(BuildingParameter))]
    public class BuildingParameterDrawer : ParameterDrawer<BuildingParameter>
    {
    }

    #endregion PARAMETER

    [CustomPropertyDrawer(typeof(UnitParameterAmount))]
    [CustomPropertyDrawer(typeof(BuildingParameterAmount))]
    public class ParameterAmountDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var typeRect = new Rect(position.x, position.y, position.width / 2, position.height);
            var amountRect = new Rect(position.x + position.width / 2, position.y, position.width / 2, position.height);
            
            var parameter = (Parameter)property.FindPropertyRelative("Parameter").objectReferenceValue;
            var amount = property.FindPropertyRelative("Amount").floatValue;

            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("Parameter"), new GUIContent());

            if (parameter != null)
            {
                if (parameter.ClampMinValue && parameter.ClampMaxValue)
                    amount = EditorGUI.Slider(amountRect, amount, parameter.MinValue, parameter.MaxValue);
                else
                    amount = EditorGUI.FloatField(amountRect, new GUIContent(), amount);

                if (parameter.IsIntegral)
                    amount = Mathf.RoundToInt(amount);
            }

            EditorGUI.EndProperty();

            property.FindPropertyRelative("Amount").floatValue = amount;
        }
    }
}