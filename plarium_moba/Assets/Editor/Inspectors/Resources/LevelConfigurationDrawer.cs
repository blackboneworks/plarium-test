﻿using Models.Buildings;
using Models.Units;
using UnityEditor;
using UnityEngine;

namespace Inspectors.Resources
{
    [CustomPropertyDrawer(typeof(HeroLevelConfiguration))]
    public class HeroLevelConfigurationDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight * 3 + EditorGUI.GetPropertyHeight(property.FindPropertyRelative("Adjusts")); 
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var levelPosition = new Rect(position.x, position.y, position.width - 10, EditorGUIUtility.singleLineHeight);
            var xpPosition = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, position.width - 10, EditorGUIUtility.singleLineHeight);
            var respawnTimePosition = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 2, position.width - 10, EditorGUIUtility.singleLineHeight);
            var adjustsPosition = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 3, position.width - 10, EditorGUI.GetPropertyHeight(property.FindPropertyRelative("Adjusts")));

            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.DrawRect(position, new Color(1,1,1, 0.1f));
            EditorGUI.DrawRect(new Rect(position.x + 1, position.y + 1, position.width - 2, position.height - 2), new Color(0,0,0, 0.3f));

            EditorGUI.LabelField(levelPosition, "Level " + property.FindPropertyRelative("Level").intValue);
            EditorGUI.PropertyField(xpPosition, property.FindPropertyRelative("Experience"));
            EditorGUI.PropertyField(respawnTimePosition, property.FindPropertyRelative("RespawnTime"));
            EditorGUI.PropertyField(adjustsPosition, property.FindPropertyRelative("Adjusts"), true);
            
            EditorGUI.EndProperty();
        }
    }

    [CustomPropertyDrawer(typeof(BarrackLevelConfiguration))]
    public class BarrackLevelConfigurationDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight * 2 + 
                EditorGUI.GetPropertyHeight(property.FindPropertyRelative("BuildingParametersChanges")) + 
                EditorGUI.GetPropertyHeight(property.FindPropertyRelative("UnitParametersChanges"));
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var levelPosition = new Rect(position.x, position.y, position.width - 10, EditorGUIUtility.singleLineHeight);
            var xpPosition = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, position.width - 10, EditorGUIUtility.singleLineHeight);
            var buildingParametersPosition = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 2, position.width - 10, EditorGUI.GetPropertyHeight(property.FindPropertyRelative("BuildingParametersChanges")));
            var unitParametersPosition = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 2 + EditorGUI.GetPropertyHeight(property.FindPropertyRelative("BuildingParametersChanges")), position.width - 10, EditorGUI.GetPropertyHeight(property.FindPropertyRelative("BuildingParametersChanges")));

            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.DrawRect(position, new Color(1, 1, 1, 0.1f));
            EditorGUI.DrawRect(new Rect(position.x + 1, position.y + 1, position.width - 2, position.height - 2), new Color(0, 0, 0, 0.3f));

            EditorGUI.LabelField(levelPosition, "Level " + property.FindPropertyRelative("Level").intValue);
            EditorGUI.PropertyField(xpPosition, property.FindPropertyRelative("Price"));
            EditorGUI.PropertyField(buildingParametersPosition, property.FindPropertyRelative("BuildingParametersChanges"), true);
            EditorGUI.PropertyField(unitParametersPosition, property.FindPropertyRelative("UnitParametersChanges"), true);

            EditorGUI.EndProperty();
        }
    }
}
