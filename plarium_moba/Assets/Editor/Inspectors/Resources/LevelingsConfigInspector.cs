﻿using Models.Buildings;
using Models.Units;
using UnityEditor;

namespace Inspectors.Resources
{
    [CustomEditor(typeof(HeroConfig))]
    public class HeroConfigInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var config = target as HeroConfig;
            if(config == null) return;

            for (int i = 0; i < config.Levels.Length; i++)
                config.Levels[i].Level = i + 1;
        }
    }

    [CustomEditor(typeof(BarrackConfig))]
    public class BarrackConfigInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var config = target as BarrackConfig;
            if (config == null) return;

            for (int i = 0; i < config.LevelConfigs.Length; i++)
                config.LevelConfigs[i].Level = i + 1;
        }
    }
}