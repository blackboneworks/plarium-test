﻿using System.Linq;
using Models;
using UnityEditor;
using UnityEngine;

namespace Inspectors.Resources
{
    [CustomPropertyDrawer(typeof(Team))]
    public class TeamDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var teams = Team.GetAllTeams().ToList();
            EditorGUI.BeginProperty(position, label, property);

            var index = EditorGUI.Popup(position, teams.IndexOf((Team)property.objectReferenceValue),
                teams.Select(x => x.name).ToArray());

            if ((index < 0) || (index >= teams.Count)) index = 0;

            if (teams.Count > 0)
                property.objectReferenceValue = teams[index];

            EditorGUI.EndProperty();
        }
    }
}
