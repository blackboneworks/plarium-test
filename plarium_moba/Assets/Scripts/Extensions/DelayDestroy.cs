﻿using UnityEngine;

public class DelayDestroy : MonoBehaviour
{
    [SerializeField] private float _delay = 3f;

	// Use this for initialization
	void Start ()
    {
        Destroy(gameObject, _delay);
	}
}
