﻿using System.Linq;
using Controllers.Game.Units;
using UnityEngine;

namespace Extensions.Debug
{
    public class UnitGUIDebug : MonoBehaviour
    {
        private UnitController _unit;
        void OnGUI()
        {
            if (_unit == null) _unit = GetComponent<UnitController>();
            if (_unit == null) return;

            GUI.Window(GetInstanceID(), new Rect(50, 50, 250, 400), DrawWindow, name);
        }

        private void DrawWindow(int id)
        {
            GUI.DragWindow();

            var parameters = _unit.GetParameters();
            GUILayout.BeginVertical();
            for (int i = 0; i < parameters.Count; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(parameters.ElementAt(i).Key.name);
                GUILayout.Label(parameters.ElementAt(i).Value.ToString());
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }
    }
}
