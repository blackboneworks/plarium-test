﻿using System;
using System.Collections;
using UnityEngine;

public static class MonoBehaviourExtensions
{
    public static Coroutine Invoke<T>(this T behaviour, Action call, float delay) where T : MonoBehaviour
    {
        return behaviour.StartCoroutine(InvokeRoutine(call, delay));
    }

    private static IEnumerator InvokeRoutine(Action call, float delay)
    {
        yield return new WaitForSeconds(delay);
        call.Invoke();
    }

    public static Coroutine Invoke<T>(this T behaviour, Action<T> call, float delay) where T : MonoBehaviour
    {
        return behaviour.StartCoroutine(InvokeRoutine(behaviour, call, delay));
    }

    private static IEnumerator InvokeRoutine<T>(T target, Action<T> call, float delay)
    {
        yield return new WaitForSeconds(delay);
        call.Invoke(target);
    }

}