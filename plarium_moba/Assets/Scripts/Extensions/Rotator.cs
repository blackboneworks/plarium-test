﻿using UnityEngine;

namespace Extensions
{
    public class Rotator : MonoBehaviour
    {
        [SerializeField] private Vector3 _rotation;

        // Update is called once per frame
        void Update()
        {
            transform.Rotate(_rotation * Time.deltaTime, Space.Self);
        }
    }
}
