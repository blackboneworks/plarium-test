﻿using System;
using System.Collections.Generic;
using Controllers.Game;
using Controllers.Game.Units;
using Controllers.Game.Units.Abilities.Attack;
using Controllers.Selection;
using UnityEngine;

namespace Extensions
{
    /// <summary>
    /// Global events manager, aggregates all events
    /// </summary>
    public static class EventManager
    {
        public static GameEvents Game = new GameEvents();
        public static ProgressEvents Progress = new ProgressEvents();
        public static UnitsEvents Units = new UnitsEvents();
        public static HeroEvents Hero = new HeroEvents();
        public static InputEvents Input = new InputEvents();

        #region Game Events

        public sealed class GameEvents
        {
            public EventHolder<float> GameStarged = new EventHolder<float>();
            public EventHolder<GameSettings> GameInitialized = new EventHolder<GameSettings>();
            public EventHolder PreparationEnded = new EventHolder();

            public EventHolder<float> EnemyWaveCountdownStarted = new EventHolder<float>();
            public EventHolder<int> EnemyWaveStarted = new EventHolder<int>();
            public EventHolder<int> EnemyWaveCompleted = new EventHolder<int>();

            public EventHolder AllEnemyWavesCompleted = new EventHolder();
            public EventHolder PlayerBaseDestroyed = new EventHolder();

            public EventHolder<bool> GameEnded = new EventHolder<bool>();
        }

        #endregion Game Events

        #region Progress Events

        public sealed class ProgressEvents
        {
            public EventHolder<int> GoldAmountChanged = new EventHolder<int>();
            public EventHolder<int> XPChanged = new EventHolder<int>();
            public EventHolder<int> HeroLevelChanged = new EventHolder<int>();
        }

        #endregion Game Events

        #region Units Events

        public sealed class UnitsEvents
        {
            public EventHolder<UnitController> UnitSpawned = new EventHolder<UnitController>();
            public EventHolder<UnitController> AllyUnitSpawned = new EventHolder<UnitController>();
            public EventHolder<UnitController> EnemyUnitSpawned = new EventHolder<UnitController>();

            public EventHolder<UnitController, AttackData> UnitKilled = new EventHolder<UnitController, AttackData>();

            public EventHolder<UnitController, AttackData> AllyUnitKilled =
                new EventHolder<UnitController, AttackData>();

            public EventHolder<UnitController, AttackData> EnemyUnitKilled =
                new EventHolder<UnitController, AttackData>();
        }

        #endregion Units Events

        #region Hero Events

        public sealed class HeroEvents
        {
            public EventHolder<UnitController> HeroSpawned = new EventHolder<UnitController>();
            public EventHolder<UnitController, AttackData> HeroKilled = new EventHolder<UnitController, AttackData>();
            public EventHolder<float> HeroRespawnCountdownStarted = new EventHolder<float>();
        }

        #endregion Hero Events

        #region Input Events

        public sealed class InputEvents
        {
            public EventHolder<Selectable> SigleSelected = new EventHolder<Selectable>();
            public EventHolder<IEnumerable<Selectable>> GroupSelected = new EventHolder<IEnumerable<Selectable>>();
            public EventHolder<RaycastHit> ActionQueried = new EventHolder<RaycastHit>();

            public EventHolder Applied = new EventHolder();
            public EventHolder Denied = new EventHolder();

            public EventHolder<int> AlphaKey = new EventHolder<int>();
        }

        #endregion Input Events

        public static void Flush()
        {
            Game = new GameEvents();
            Progress = new ProgressEvents();
            Units = new UnitsEvents();
            Hero = new HeroEvents();
            Input = new InputEvents();
        }
    }

    #region Event Holders

    public class EventHolder
    {
        public event Action OnEvent;

        public void Invoke()
        {
            if (OnEvent != null)
                OnEvent();
        }

        public void SafeInvoke()
        {
            try
            {
                Invoke();
            }
            catch
            {
            }
        }

        public bool isEmpty()
        {
            return OnEvent == null;
        }
    }

    public class EventHolder<T>
    {
        public event Action<T> OnEvent;

        public void Invoke(T arg1)
        {
            if (OnEvent != null)
                OnEvent(arg1);
        }

        public void SafeInvoke(T arg1)
        {
            try
            {
                Invoke(arg1);
            }
            catch
            {
            }
        }

        public bool isEmpty()
        {
            return OnEvent == null;
        }
    }

    public class EventHolder<T1, T2>
    {
        public event Action<T1, T2> OnEvent;

        public void Invoke(T1 arg1, T2 arg2)
        {
            if (OnEvent != null)
                OnEvent(arg1, arg2);
        }

        public void SafeInvoke(T1 arg1, T2 arg2)
        {
            try
            {
                Invoke(arg1, arg2);
            }
            catch
            {
            }
        }

        public bool isEmpty()
        {
            return OnEvent == null;
        }
    }

    public class EventHolder<T1, T2, T3>
    {
        public event Action<T1, T2, T3> OnEvent;

        public void Invoke(T1 arg1, T2 arg2, T3 arg3)
        {
            if (OnEvent != null)
                OnEvent(arg1, arg2, arg3);
        }

        public void SafeInvoke(T1 arg1, T2 arg2, T3 arg3)
        {
            try
            {
                Invoke(arg1, arg2, arg3);
            }
            catch
            {
            }
        }

        public bool isEmpty()
        {
            return OnEvent == null;
        }
    }

    public class EventHolder<T1, T2, T3, T4>
    {
        public event Action<T1, T2, T3, T4> OnEvent;

        public void Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            if (OnEvent != null)
                OnEvent(arg1, arg2, arg3, arg4);
        }

        public void SafeInvoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            try
            {
                Invoke(arg1, arg2, arg3, arg4);
            }
            catch
            {
            }
        }

        public bool isEmpty()
        {
            return OnEvent == null;
        }
    }

    #endregion
}