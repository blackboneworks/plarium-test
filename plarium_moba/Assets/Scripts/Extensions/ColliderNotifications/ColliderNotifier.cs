﻿using UnityEngine;

namespace Extensions
{
    #region Basic
    // not inherited from ColliderNotifier'2 cause collision class necesseries
    public class ColliderNotifier : MonoBehaviour
    {
        public event TriggerHandler<Collider> TriggerEnterEvent;
        public event TriggerHandler<Collider> TriggerStayEvent;
        public event TriggerHandler<Collider> TriggerExitEvent;

        public event CollisionHandler<Collision> CollisionEnterEvent;
        public event CollisionHandler<Collision> CollisionStayEvent;
        public event CollisionHandler<Collision> CollisionExitEvent;

        public virtual void OnTriggerEnter(Collider other)
        {
            if (TriggerEnterEvent != null)
                TriggerEnterEvent.Invoke(other);
        }

        public virtual void OnTriggerStay(Collider other)
        {
            if (TriggerStayEvent != null)
                TriggerStayEvent.Invoke(other);
        }

        public virtual void OnTriggerExit(Collider other)
        {
            if (TriggerExitEvent != null)
                TriggerExitEvent.Invoke(other);
        }

        public virtual void OnCollisionEnter(Collision collision)
        {
            if (CollisionEnterEvent != null)
                CollisionEnterEvent.Invoke(collision);
        }

        public virtual void OnCollisionStay(Collision collision)
        {
            if (CollisionStayEvent != null)
                CollisionStayEvent.Invoke(collision);
        }

        public virtual void OnCollisionExit(Collision collision)
        {
            if (CollisionExitEvent != null)
                CollisionExitEvent.Invoke(collision);
        }
    }

    #endregion Basic

}