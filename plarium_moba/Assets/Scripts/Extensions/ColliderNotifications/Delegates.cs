﻿namespace Extensions
{
    public delegate void TriggerHandler<in T>(T other);
    public delegate void CollisionHandler<in T>(T collision);
}