﻿using UnityEngine;

namespace Extensions
{
    #region Generic`2
    public class ColliderNotifier<TTrigger, TCollider> : ColliderNotifier where TTrigger : Component where TCollider : Component
    {
        public new event TriggerHandler<TTrigger> TriggerEnterEvent;
        public new event TriggerHandler<TTrigger> TriggerStayEvent;
        public new event TriggerHandler<TTrigger> TriggerExitEvent;

        public new event CollisionHandler<TCollider> CollisionEnterEvent;
        public new event CollisionHandler<TCollider> CollisionStayEvent;
        public new event CollisionHandler<TCollider> CollisionExitEvent;

        public override void OnTriggerEnter(Collider other)
        {
            if (TriggerEnterEvent != null)
                TriggerEnterEvent.Invoke(other.GetComponent<TTrigger>());
        }

        public override void OnTriggerStay(Collider other)
        {
            if (TriggerStayEvent != null)
                TriggerStayEvent.Invoke(other.GetComponent<TTrigger>());
        }

        public override void OnTriggerExit(Collider other)
        {
            if (TriggerExitEvent != null)
                TriggerExitEvent.Invoke(other.GetComponent<TTrigger>());
        }

        public override void OnCollisionEnter(Collision collision)
        {
            if (CollisionEnterEvent != null)
                CollisionEnterEvent.Invoke(collision.collider.GetComponent<TCollider>());
        }

        public override void OnCollisionStay(Collision collision)
        {
            if (CollisionStayEvent != null)
                CollisionStayEvent.Invoke(collision.collider.GetComponent<TCollider>());
        }

        public override void OnCollisionExit(Collision collision)
        {
            if (CollisionExitEvent != null)
                CollisionExitEvent.Invoke(collision.collider.GetComponent<TCollider>());
        }
    }
    #endregion Generic`2
}