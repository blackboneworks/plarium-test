﻿using UnityEngine;

namespace Extensions
{
    #region Generic`1
    public class ColliderNotifier<T> : ColliderNotifier where T : Component
    {
        public new event TriggerHandler<T> TriggerEnterEvent;
        public new event TriggerHandler<T> TriggerStayEvent;
        public new event TriggerHandler<T> TriggerExitEvent;

        public new event CollisionHandler<T> CollisionEnterEvent;
        public new event CollisionHandler<T> CollisionStayEvent;
        public new event CollisionHandler<T> CollisionExitEvent;

        public override void OnTriggerEnter(Collider other)
        {
            if (TriggerEnterEvent != null)
                TriggerEnterEvent.Invoke(other.GetComponent<T>());
        }

        public override void OnTriggerStay(Collider other)
        {
            if (TriggerStayEvent != null)
                TriggerStayEvent.Invoke(other.GetComponent<T>());
        }

        public override void OnTriggerExit(Collider other)
        {
            if (TriggerExitEvent != null)
                TriggerExitEvent.Invoke(other.GetComponent<T>());
        }

        public override void OnCollisionEnter(Collision collision)
        {
            if (CollisionEnterEvent != null)
                CollisionEnterEvent.Invoke(collision.collider.GetComponent<T>());
        }

        public override void OnCollisionStay(Collision collision)
        {
            if (CollisionStayEvent != null)
                CollisionStayEvent.Invoke(collision.collider.GetComponent<T>());
        }

        public override void OnCollisionExit(Collision collision)
        {
            if (CollisionExitEvent != null)
                CollisionExitEvent.Invoke(collision.collider.GetComponent<T>());
        }
    }
    #endregion Generic`1

}