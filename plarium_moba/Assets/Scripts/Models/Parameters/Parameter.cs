﻿using UnityEngine;

namespace Models.Parameters
{
    public abstract class Parameter : ScriptableObject
    {
        protected const string PARAMETERS_ROOT = "Parameters/";

        [Header("Base settings")]
        public string Name;
        public bool IsIntegral;
        public float DefaultValue;
        public Sprite Icon;
        [TextArea] public string Description;

        [Header("Clamping")]
        public bool ClampMinValue;
        public float MinValue = 0f;

        public bool ClampMaxValue;
        public float MaxValue = 0f;

        public static Parameter GetParameter(string name)
        {
            return Resources.Load<Parameter>(PARAMETERS_ROOT + name);
        }

        public static Parameter[] GetAllParameters()
        {
            return Resources.LoadAll<Parameter>(PARAMETERS_ROOT);
        }
        
    }
}