﻿using UnityEngine;

namespace Models.Parameters
{
    /// <summary>
    /// Units parameter presentation class.
    /// </summary>
    [CreateAssetMenu(
         menuName = "Models/Units/Unit Parameter",
         fileName = "UnitParameter",
         order = 555)]
    public class UnitParameter : Parameter
    {
        protected const string UNIT_PARAMETERS_ROOT = PARAMETERS_ROOT + "Units/";

        public new static UnitParameter GetParameter(string name)
        {
            return Resources.Load<UnitParameter>(UNIT_PARAMETERS_ROOT + name);
        }
        public new static UnitParameter[] GetAllParameters()
        {
            return Resources.LoadAll<UnitParameter>(UNIT_PARAMETERS_ROOT);
        }

        #region Properties accessing to parameters resources

        public static UnitParameter HP { get { return GetParameter("HP"); } }
        public static UnitParameter Armor { get { return GetParameter("Armor"); } }
        public static UnitParameter Attack { get { return GetParameter("Attack"); } }
        public static UnitParameter AttackSpeed { get { return GetParameter("AttackSpeed"); } }
        public static UnitParameter AttackRange { get { return GetParameter("AttackRange"); } }
        public static UnitParameter Speed { get { return GetParameter("Speed"); } }
        public static UnitParameter RewardXP { get { return GetParameter("RewardXP"); } }
        public static UnitParameter RewardGold { get { return GetParameter("RewardGold"); } }

        #endregion Properties accessing to parameters resources
    }
}