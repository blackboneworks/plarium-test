﻿using System;

namespace Models.Parameters
{
    [Serializable]
    public class ParameterAmount<T> where T : Parameter
    {
        public T Parameter;
        public float Amount;
    }
}