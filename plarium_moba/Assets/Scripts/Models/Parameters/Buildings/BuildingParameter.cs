﻿using UnityEngine;

namespace Models.Parameters
{
    /// <summary>
    /// Units parameter presentation class.
    /// </summary>
    [CreateAssetMenu(
         menuName = "Models/Units/Building Parameter",
         fileName = "BuildingParameter",
         order = 555)]
    public class BuildingParameter : Parameter
    {
        protected const string BUILDINGS_PARAMETERS_ROOT = PARAMETERS_ROOT + "Buildings/";

        public new static BuildingParameter GetParameter(string name)
        {
            return Resources.Load<BuildingParameter>(BUILDINGS_PARAMETERS_ROOT + name);
        }

        public new static BuildingParameter[] GetAllParameters()
        {
            return Resources.LoadAll<BuildingParameter>(BUILDINGS_PARAMETERS_ROOT);
        }

        #region Properties accessing to parameters resources

        public static BuildingParameter TrainingSpeed { get { return GetParameter("TrainingSpeed"); } }

        #endregion Properties accessing to parameters resources
    }
}