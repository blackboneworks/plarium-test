﻿using System;
using System.Linq;
using Models.Parameters;
using Models.Units;
using UnityEngine;

namespace Models.Buildings
{
    [CreateAssetMenu(
         menuName = "Models/Buildings/Barrack",
         fileName = "Barrack",
         order = 555)]
    public class BarrackConfig : ScriptableObject
    {
        [Header("Unit")]
        public UnitConfig UnitConfig;

        [Header("Leveling Parameters")]
        public BarrackLevelConfiguration[] LevelConfigs;
        
        public float GetLeveledParameter(BuildingParameter parameter, int level)
        {
            var parameterAmount = LevelConfigs[level - 1].BuildingParametersChanges.FirstOrDefault(x => x.Parameter == parameter);
            if (parameterAmount != null) return parameterAmount.Amount;
            return 0f;
        }
    }

    [Serializable]
    public class BarrackLevelConfiguration
    {
        public int Level;
        public int Price;
        public BuildingParameterAmount[] BuildingParametersChanges;
        public UnitParameterAmount[] UnitParametersChanges;
    }
}
