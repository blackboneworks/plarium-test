﻿using UnityEngine;

namespace Models.Buildings
{
    [CreateAssetMenu(
         menuName = "Models/Buildings/Sofa",
         fileName = "Sofa",
         order = 555)]
    public class SofaConfig : ScriptableObject
    {
        public float HP = 5000;
    }
}