﻿using Models.Units;
using UnityEngine;

namespace Models.Buildings
{
    [CreateAssetMenu(
         menuName = "Models/Buildings/Unit Builder Config",
         fileName = "UnitBuilderConfig",
         order = 555)]
    public class UnitBuilderConfig : MonoBehaviour
    {
        [Header("Unit")]
        public UnitConfig UnitConfig;
    }
}
