﻿using System;
using Models.Units;
using UnityEngine;

namespace Models.Buildings
{
    [CreateAssetMenu(
         menuName = "Models/Buildings/Fountain",
         fileName = "Fountain",
         order = 555)]
    public class FountainConfig : ScriptableObject
    {
        public HeroConfig Unit;
        public UnitRegenerationAmount[] BuffAreaBindings;
    }

    [Serializable]
    public class UnitRegenerationAmount
    {
        public UnitConfig Unit;
        public float Amount;
        public float Period;
    }
}