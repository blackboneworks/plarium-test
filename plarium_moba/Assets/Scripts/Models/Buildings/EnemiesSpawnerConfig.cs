﻿using System;
using Models.Units;
using UnityEngine;

namespace Models.Buildings
{
    [CreateAssetMenu(
         menuName = "Models/Buildings/Enemies Spawner",
         fileName = "Enemies Spawner",
         order = 555)]
    public class EnemiesSpawnerConfig : ScriptableObject
    {
        public EnemiesWave[] Waves;
    }

    [Serializable]
    public class EnemiesWave
    {
        public float Delay;
        public float PerUnitDelay;
        public UnitAmount[] Units;
    }

    [Serializable]
    public class UnitAmount
    {
        public UnitConfig Unit;
        public int Amount;
    }
}