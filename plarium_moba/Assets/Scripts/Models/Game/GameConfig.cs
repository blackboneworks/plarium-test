﻿using UnityEngine;

namespace Models
{
    [CreateAssetMenu(
         menuName = "Models/Game Config",
         fileName = "GameConfig",
         order = 555)]
    public class GameConfig : ScriptableObject
    {
        public Team PlayerTeam;
        public float SpawnersStartTime = 30f;
        public int StartGold = 300;
    }
}