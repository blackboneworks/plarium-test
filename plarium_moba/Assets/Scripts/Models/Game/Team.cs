﻿using UnityEngine;

namespace Models {
     [CreateAssetMenu(
         menuName = "Models/Team",
         order = 555)]
    public class Team : ScriptableObject
    {
        public string Name;
        public Color Color;

        
        protected const string TEAMS_ROOT = "Teams/";

        public static Team GetTeam(string name)
        {
            return Resources.Load<Team>(TEAMS_ROOT + name);
        }
        public static Team[] GetAllTeams()
        {
            return Resources.LoadAll<Team>(TEAMS_ROOT);
        }
    }
}