﻿using System.Linq;
using Controllers.Game.Buldings;
using Controllers.Game.Units;
using Controllers.Game.Units.Abilities.Attack;
using UnityEngine;

namespace Models.Units.Behaviour
{
    public class UnitBehaviour : ScriptableObject
    {
        protected UnitController _unit;
        protected IAttackable _target;
        protected Vector3? _moveTarget;

        public virtual void Initialize(UnitController unit)
        {
            _unit = unit;
        }

        public virtual void Update()
        {
        }

        public virtual void SetAttackTarget(IAttackable target)
        {
            _moveTarget = null;
            _target = target;
            _unit.BehaviourController.CurrentTarget = target;
        }

        public virtual void SetMoveTarget(Vector3 moveTartget)
        {
            _moveTarget = moveTartget;
        }

        protected void MoveToTarget(Vector3 moveTarget)
        {
            _unit.NavigationAgent.Resume();
            _unit.NavigationAgent.SetDestination(moveTarget);
            _unit.BehaviourController.IsMoving = true;
        }

        protected void Stop()
        {
            _unit.NavigationAgent.Stop();
            _unit.BehaviourController.IsMoving = false;
        }

        protected void Attack(IAttackable target, int abilityIndex = 0)
        {
            if (_unit.AbilitiesHub.CanUseAbility(abilityIndex))
                _unit.AbilitiesHub.UseAbility(abilityIndex, target.gameObject);
        }

        protected IAttackable FindTarget()
        {
            // find target in attack range
            var target = FindTargetInRange(_unit.transform.position,
                _unit.AbilitiesHub.GetAblilityAttackRange());

            if (target != null) return target;

            target = FindTargetInRange(_unit.transform.position, 9999);
            return target;
        }

        protected void CheckoutTarget()
        {
            if (Vector3.Distance(_target.transform.position, _unit.transform.position) >
                _unit.AbilitiesHub.GetAblilityAttackRange() * 3)
                SetAttackTarget(FindTarget());

            _unit.Invoke(CheckoutTarget, 1f);
        }

        protected Vector3 FindIdleSpot()
        {
            return GameObject.FindObjectsOfType<IdleSpot>()
                .Where(x => x.TeamControl.Team == _unit.TeamControl.Team)
                .Select(x => x.GetPoint())
                .OrderBy(x => Vector3.Distance(x, _unit.transform.position))
                .FirstOrDefault();
        }

        private IAttackable FindTargetInRange(Vector3 position, float radius)
        {
            return Physics.OverlapSphere(position, radius)
                .Where(x => x.GetComponent(typeof(IAttackable)) != null)
                .Select(x => x.GetComponent(typeof(IAttackable)) as IAttackable)
                .Where(x => x.CanBeAttackedBy(_unit))
                .OrderBy(x => Vector3.Distance(x.transform.position, _unit.transform.position))
                .FirstOrDefault();
        }
    }
}