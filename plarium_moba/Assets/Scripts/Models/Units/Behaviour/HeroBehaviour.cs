﻿using Controllers.Game.Units;
using Extensions;
using UnityEngine;

namespace Models.Units.Behaviour
{
    [CreateAssetMenu(
         menuName = "Models/Units/Behaviour/Hero Behaviour",
         order = 555)]
    public class HeroBehaviour : UnitBehaviour
    {
        public override void Initialize(UnitController unit)
        {
            base.Initialize(unit);

            EventManager.Input.AlphaKey.OnEvent += OnAbilityKeyPressed;
        }

        private void OnDestroy()
        {
            EventManager.Input.AlphaKey.OnEvent -= OnAbilityKeyPressed;
        }

        private void OnAbilityKeyPressed(int number)
        {
            if (number == 0 || number >= _unit.AbilitiesHub.AbilitiesCount) return;
            if(!_unit.AbilitiesHub.CanUseAbility(number)) return;

            _unit.AbilitiesHub.UseAbility(number);
        }


        public override void Update()
        {
            if(_unit == null) return;

            if (_moveTarget != null)
            {
                if (Vector3.Distance(_unit.transform.position, _moveTarget.Value) < 0.5f)
                {
                    Stop();
                }
                else
                    MoveToTarget(_moveTarget.Value);
                return;
            }

            if (_target != null && (Component)_target != null)
            {
                if (Vector3.Distance(_unit.transform.position, _target.transform.position) > _unit.AbilitiesHub.GetAblilityAttackRange())
                    MoveToTarget(_target.transform.position);
                else
                {
                    Stop();
                    Attack(_target, 0);
                }
                return;
            }
        }
    }
}