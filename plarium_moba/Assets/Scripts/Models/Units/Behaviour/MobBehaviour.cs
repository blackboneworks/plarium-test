﻿using Controllers.Game.Units;
using UnityEngine;

namespace Models.Units.Behaviour
{
    [CreateAssetMenu(
         menuName = "Models/Units/Behaviour/Mob Behaviour",
         order = 555)]
    public class MobBehaviour : UnitBehaviour
    {
        [SerializeField] private float _targetCheckInterval = 1f;
        private float _lastCheckTime;
        public override void Initialize(UnitController unit)
        {
            base.Initialize(unit);
            SetAttackTarget(FindTarget());
        }

        public override void Update()
        {
            if (_unit == null) return;
            
            if (_lastCheckTime + _targetCheckInterval < Time.time)
            {
                SetAttackTarget(FindTarget());

                _lastCheckTime = Time.time;
                return;
            }

            if (_target != null && (Component)_target != null)
            {
                if (Vector3.Distance(_unit.transform.position, _target.transform.position) >
                    _unit.AbilitiesHub.GetAblilityAttackRange())
                {
                    SetAttackTarget(FindTarget());
                    MoveToTarget(_target.transform.position);
                }
                else
                {
                    Stop();
                    Attack(_target, 0);
                }
                return;
            }

            if (_moveTarget != null)
            {
                if (Vector3.Distance(_unit.transform.position, _moveTarget.Value) < 1)
                    Stop();
                else
                    MoveToTarget(_moveTarget.Value);
                return;
            }

        }
    }
}