﻿using System;
using Models.Parameters;
using UnityEngine;

namespace Models.Units
{
    [CreateAssetMenu(
         menuName = "Models/Units/Hero",
         fileName = "Hero",
         order = 555)]
    public class HeroConfig : UnitConfig
    {
        [Header("Leveling Parameters")]
        public HeroLevelConfiguration[] Levels;

        [Header("Specific View")]
        public GameObject LevelUpEffect;

        public float Get(UnitParameter parameter, int level)
        {
            level = Mathf.Min(level - 2, Levels.Length);
            var baseValue = Get(parameter);
            for (int i = 0; i < level; i++)
                foreach (var adjust in Levels[i].Adjusts)
                    if (adjust.Parameter == parameter)
                        baseValue += adjust.Amount;
            return baseValue;
        }
    }

    [Serializable]
    public class HeroLevelConfiguration
    {
        public int Level;
        public int Experience;
        public float RespawnTime;
        public UnitParameterAmount[] Adjusts;
    }
}