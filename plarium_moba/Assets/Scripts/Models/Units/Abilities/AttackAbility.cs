﻿using System;
using Controllers.Game.Units;
using Controllers.Game.Units.Abilities.Attack;
using Models.Parameters;
using UnityEngine;

namespace Models.Units.Abilities
{
    [CreateAssetMenu(
         menuName = "Models/Units/Abilities/Attack Ability",
         order = 555)]
    public class AttackAbility : UnitAbility
    {
        public override bool Validate(UnitController unit, GameObject target)
        {
            // can not use abilities with no parameters
            if (unit.GetParameter(UnitParameter.Attack) <= 0f) return false;
            if (unit.GetParameter(UnitParameter.AttackRange) <= 0f) return false;
            if (unit.GetParameter(UnitParameter.AttackSpeed) <= 0f) return false;

            var attackable = target.GetComponent(typeof(IAttackable)) as IAttackable;

            // cant not attack if target is not an attackable target
            // or target is already dead
            if (attackable == null) return false;
            if (!attackable.CanBeAttackedBy(unit)) return false;
            return true;
        }

        public override void ExecuteAbility(UnitController unit, GameObject target, Action<bool> callback = null)
        {
            if (!Validate(unit, target)) return;

            var attackable = target.GetComponent(typeof(IAttackable)) as IAttackable;
            attackable.Attacked(new AttackData(unit, unit.GetParameter(UnitParameter.Attack)));
            
            if(callback != null) callback(true);
        }
    }
}