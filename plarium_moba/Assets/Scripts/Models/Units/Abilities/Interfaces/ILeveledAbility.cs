﻿using System;

namespace Models.Units.Abilities
{
    public interface ILeveledAbility
    {
        bool CanBeUpgraded { get; }
        void LevelUp();
        int Level { get; }
        event Action<int> AbilityLeveledUp;
    }
}