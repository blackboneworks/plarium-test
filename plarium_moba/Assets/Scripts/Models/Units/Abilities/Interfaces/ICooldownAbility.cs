﻿namespace Models.Units.Abilities
{
    public interface ICooldownAbility
    {
        float Cooldown { get; }
    }
}