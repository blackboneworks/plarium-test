﻿namespace Models.Units.Abilities
{
    public interface ICustomRangeAbility
    {
        float Range { get; }
    }
}