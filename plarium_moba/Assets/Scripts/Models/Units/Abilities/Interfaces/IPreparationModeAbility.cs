﻿namespace Models.Units.Abilities.Interfaces
{
    public interface IPreparationModeAbility
    {
        void Update();
        void Applied();
        void Denied();
    }
}