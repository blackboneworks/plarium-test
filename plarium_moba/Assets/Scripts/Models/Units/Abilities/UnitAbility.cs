﻿using System;
using Controllers.Game.Units;
using UnityEngine;

namespace Models.Units.Abilities
{
    public abstract class UnitAbility : ScriptableObject
    {
        public string Name;
        public string Description;
        public Sprite Icon;
        
        public float LastUsedTime { get; set; }

        public abstract bool Validate(UnitController unit, GameObject target);
        public abstract void ExecuteAbility(UnitController unit, GameObject target, Action<bool> callback = null);
    }

    public abstract class ParametrizedUnitAbility<T> : UnitAbility where T : AbilityParameters
    {
        public T[] Parameters;
    }

    [Serializable]
    public class AbilityParameters
    {
    }
}