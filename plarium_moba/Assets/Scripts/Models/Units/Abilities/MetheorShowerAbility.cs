﻿using System;
using System.Linq;
using Controllers.Game.Units;
using Controllers.Game.Units.Abilities.Attack;
using Controllers.Selection;
using Extensions;
using Models.Units.Abilities.Interfaces;
using UnityEngine;

namespace Models.Units.Abilities
{
    [CreateAssetMenu(
         menuName = "Models/Units/Abilities/Metheor Shower Ability",
         order = 555)]
    public class MetheorShowerAbility : UnitAbility, ICooldownAbility, ILeveledAbility, IPreparationModeAbility
    {
        [SerializeField] private MetheorShowerAbilityLevelData[] _abilityLevelData;
        [SerializeField] private GameObject _pointerPrefab;

        private int _level = 1;
        private Action<bool> _callback;
        private Transform _pointer;
        private RaycastHit _hit;
        private UnitController _unit;

        public event Action<int> AbilityLeveledUp = delegate { };

        public bool CanBeUpgraded { get { return _level < _abilityLevelData.Length; } }
        public float Cooldown { get { return _abilityLevelData[Level - 1].Cooldown; } }
        public float Radius { get { return _abilityLevelData[Level - 1].Radius; } }
        public float Damage { get { return _abilityLevelData[Level - 1].Damage; } }
        public int Level { get { return _level; } }

        public void LevelUp()
        {
            if(!CanBeUpgraded) return;
            _level++;
            AbilityLeveledUp.Invoke(Level);
        }

        public override bool Validate(UnitController unit, GameObject target)
        {
            return true;
        }

        public override void ExecuteAbility(UnitController unit, GameObject target, Action<bool> callback = null)
        {
            _unit = unit;
            _callback = callback;

            _pointer = Instantiate(_pointerPrefab).transform;
            _pointer.transform.localScale = Vector3.one * Radius;

            PointerController.SkipNextEvent = true;

            EventManager.Input.Applied.OnEvent += Applied;
            EventManager.Input.Denied.OnEvent += Denied;
        }

        public void Denied()
        {
            EventManager.Input.Applied.OnEvent -= Applied;
            EventManager.Input.Denied.OnEvent -= Denied;

            Destroy(_pointer.gameObject);

            _callback.Invoke(false);
        }

        public void Applied()
        {
            EventManager.Input.Applied.OnEvent -= Applied;
            EventManager.Input.Denied.OnEvent -= Denied;

            var enemies = Physics.OverlapSphere(_pointer.position, Radius)
                .Select(x => x.GetComponent(typeof(IAttackable)) as IAttackable)
                .Where(x => x != null)
                .Where(x => x.CanBeAttackedBy(_unit))
                .ToList();
            
            enemies.ForEach(x => x.Attacked(new AttackData(_unit, Damage)));

            Destroy(_pointer.gameObject);

            _callback.Invoke(true);
        }
        
        public void Update()
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out _hit))
                _pointer.position = _hit.point;
        }
    }

    [Serializable]
    public class MetheorShowerAbilityLevelData
    {
        public float Radius;
        public float Damage;
        public float Cooldown;
    }
}