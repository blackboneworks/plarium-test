﻿using System.Linq;
using Models.Parameters;
using Models.Units.Abilities;
using Models.Units.Behaviour;
using UnityEngine;

namespace Models.Units
{
    [CreateAssetMenu(
         menuName = "Models/Units/Unit",
         fileName = "Unit",
         order = 555)]
    public class UnitConfig : ScriptableObject
    {
        [Header("View Settings")]
        public GameObject Prefab;

        [Header("Behaviour")]
        public UnitBehaviour Behaviour;

        [Header("Abilities")]
        public UnitAbility[] Abilities;

        [Header("Model")]
        public UnitParameterAmount[] Parameters;

        public virtual float Get(UnitParameter parameter)
        {
            var parameterAmount = Parameters.FirstOrDefault(x => x.Parameter == parameter);
            if (parameterAmount != null) return parameterAmount.Amount;
            return 0f;
        }
    }
}