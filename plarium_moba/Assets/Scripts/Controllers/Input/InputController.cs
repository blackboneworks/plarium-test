﻿using System;
using Extensions;
using UnityEngine;

namespace Controllers
{
    public class InputController : MonoBehaviour
    {
        [SerializeField] private KeyCode _applyKey;
        [SerializeField] private KeyCode _altApplyKey;
        [SerializeField] private KeyCode _abortKey;
        [SerializeField] private KeyCode _altAbortKey;

        private void Update()
        {
            if(Input.GetKeyUp(_applyKey) || Input.GetKeyUp(_altApplyKey))
                EventManager.Input.Applied.Invoke();

            if (Input.GetKeyUp(_abortKey) || Input.GetKeyUp(_altAbortKey))
                EventManager.Input.Denied.Invoke();
            
            for (int i = 0; i < 9; i++)
            {
                if (Input.GetKeyUp((KeyCode)Enum.Parse(typeof(KeyCode), "Alpha" + i)))
                    EventManager.Input.AlphaKey.Invoke(i);
            }
        }
    }
}