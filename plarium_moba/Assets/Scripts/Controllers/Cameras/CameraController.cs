﻿using Controllers.Game.Units;
using Extensions;
using UnityEngine;

namespace Controllers.Cameras
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private float _distance = 30;
        [SerializeField] [Range(0, 1)] private float _shiftAreaWidth = 0.05f;
        [SerializeField] [Range(0, 10)] private float _shiftSpeed = 3;

        public Transform Target { get; set; }
        public bool FollowMode { get; private set; }

        private void Awake()
        {
            // subscribe for hero spawned event
            EventManager.Hero.HeroSpawned.OnEvent += OnHeroSpawned;
        }

        private void OnDestroy()
        {
            EventManager.Hero.HeroSpawned.OnEvent -= OnHeroSpawned;
        }

        private void OnHeroSpawned(UnitController hero)
        {
            Target = hero.transform;
            FollowMode = true;
            FollowMode = Target != null && FollowMode;
        }

        private void Update()
        {
            if (FollowMode && Target != null)
                transform.position = Target.position + -transform.forward * _distance;
            else
            {
                FollowMode = false;

                var area = Screen.height * _shiftAreaWidth;
                Vector3 offset = Vector3.zero;

                // left right shift
                if (Input.mousePosition.x < area)
                    offset = -transform.right;
                else if (Input.mousePosition.x > Screen.width - area)
                    offset = transform.right;

                // forward backward shift
                var align = transform.forward;
                align.y = 0f;
                align.Normalize();

                if (Input.mousePosition.y < area)
                    offset += -align;
                else if (Input.mousePosition.y > Screen.height - area)
                    offset += align;

                transform.position += (offset * _shiftSpeed * Time.deltaTime);
            }
        }

        public void SetFollowMode(bool value)
        {
            if(Target == null) return;

            FollowMode = value;
        }
    }
}
