﻿namespace Controllers.Selection
{
    public interface ISelectionHandler
    {
        void OnSelected();
        void OnDeselected();
    }
}