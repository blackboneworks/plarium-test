﻿using UnityEngine;

namespace Controllers.Selection
{
    [RequireComponent(typeof(ISelectionHandler))]
    public class GroupSelectable : Selectable
    {
        // This is a marke class for all group selectable things
    }
}