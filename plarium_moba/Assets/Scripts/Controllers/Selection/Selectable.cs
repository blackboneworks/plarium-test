﻿using UnityEngine;

namespace Controllers.Selection
{
    [RequireComponent(typeof(ISelectionHandler))]
    public class Selectable : MonoBehaviour
    {
        private ISelectionHandler _handler;
        public ISelectionHandler Handler
        {
            get
            {
                if (_handler == null) _handler = GetComponent(typeof(ISelectionHandler)) as ISelectionHandler;
                return _handler;
            }
        }

        public void Select()
        {
            Handler.OnSelected();
        }

        public void Deselect()
        {
            Handler.OnDeselected();
        }
    }
}