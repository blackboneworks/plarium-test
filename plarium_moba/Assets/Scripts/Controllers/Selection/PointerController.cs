﻿using System.Collections.Generic;
using System.Linq;
using Extensions;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Controllers.Selection
{
    public delegate void SelectionChangedDelegate(IEnumerable<GameObject> previousSelection, IEnumerable<GameObject> newSelection);

    public class PointerController : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] private PointerEventData.InputButton _selectionButton = PointerEventData.InputButton.Left;
        [SerializeField] private PointerEventData.InputButton _actionButton = PointerEventData.InputButton.Right;
        
        public Rect? SelectionRect
        {
            get
            {
                if (_isDrag) return _rect;
                return null;
            }
        }

        public static bool SkipNextEvent { get; set; }

        private List<Selectable> _selected = new List<Selectable>();
        private Vector2 _dragStart;
        private Rect _rect;
        private bool _isDrag;
        
        public void OnBeginDrag(PointerEventData eventData)
        {
            if (SkipNextEvent)
            {
                SkipNextEvent = false;
                return;
            }

            // left button drag only
            if (eventData.button == _selectionButton)
            {
                _isDrag = true;

                _dragStart = eventData.position;
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            // left button drag only
            if (eventData.button == _selectionButton)
            {
                _rect = Rect.MinMaxRect(
                    Mathf.Min(_dragStart.x, eventData.position.x),
                    Mathf.Min(_dragStart.y, eventData.position.y),
                    Mathf.Max(_dragStart.x, eventData.position.x),
                    Mathf.Max(_dragStart.y, eventData.position.y));
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            // left button drag only
            if (eventData.button == _selectionButton)
            {
                _isDrag = false;

                var selectables = GetGroupSelectables();
                OnSelectionChangedInternal(
                    selectables
                        .Where(x => _rect.Contains(Camera.main.WorldToScreenPoint(x.transform.position)))
                        .OfType<Selectable>()
                        .ToList());
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_isDrag) return;
            if (SkipNextEvent)
            {
                SkipNextEvent = false;
                return;
            }

            if (eventData.button == _selectionButton)
            {
                var ray = Camera.main.ScreenPointToRay(eventData.position);
                var hits = Physics.RaycastAll(ray);

                Selectable newSelection = null;
                for (int i = 0; i < hits.Length; i++)
                {
                    var c = hits[i].collider.GetComponent(typeof(Selectable));
                    newSelection = c as Selectable;

                    if (newSelection == null)
                        continue;

                    break;
                }

                OnSelectionChangedInternal(newSelection
                    ? new List<Selectable> {newSelection}
                    : new List<Selectable>());
            }

            if (eventData.button == _actionButton)
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(eventData.position), out hit))
                    EventManager.Input.ActionQueried.Invoke(hit);
            }
        }

        private GroupSelectable[] GetGroupSelectables()
        {
            return GameObject.FindObjectsOfType<GroupSelectable>();
        }

        private void OnSelectionChangedInternal(List<Selectable> newSelection)
        {
            _selected.Where(x => x != null).ToList().ForEach(x => x.Deselect());
            _selected = newSelection;
            _selected.ForEach(x => x.Select());

            EventManager.Input.GroupSelected.Invoke(newSelection);

            if (_selected.Count == 1)
                EventManager.Input.SigleSelected.Invoke(_selected[0]);
        }

    }
}
