﻿namespace Controllers.Game.Units
{
    public interface IUnitView
    {
        void Initialize(UnitController unit);
        void SetMoving(bool moving);
        void OnSelected();
        void OnDeselected();
        void AbilityUsed(int abilityIndex);
        void TargetHighlight(bool enabled);
    }
}