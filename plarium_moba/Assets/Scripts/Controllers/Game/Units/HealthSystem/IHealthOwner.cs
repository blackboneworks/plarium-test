﻿namespace Controllers.Game.Units.HealthSystem
{
    public interface IHealthOwner
    {
        HealthHub HealthHub { get; }
        float BaseHealth { get; }
    }
}