﻿using UnityEngine;

namespace Controllers.Game.Units.HealthSystem
{
    public class HealthHub : MonoBehaviour
    {
        private IHealthOwner _unit;
        private float _currentHealth;
        
        public IHealthOwner Owner { get { return _unit; } }
        public float Health { get { return _currentHealth; } }
        public float NormalizedHealth { get { return Health / _unit.BaseHealth; } }
        public bool IsAlive { get { return NormalizedHealth > 0f; } }

        public void Initialize(IHealthOwner unit)
        {
            _unit = unit;
            _currentHealth = _unit.BaseHealth;
        }
        
        public void AddHealth(float amount)
        {
            if(!IsAlive) return;
            
            _currentHealth = Mathf.Clamp(_currentHealth + amount, 0, _unit.BaseHealth);
        }
       
        public void RemoveHealth(float amount)
        {
            if(!IsAlive) return;

            _currentHealth = Mathf.Clamp(_currentHealth - amount, 0, _unit.BaseHealth);
        }

#if UNITY_EDITOR
        [ContextMenu("Kill")]
        private void TestKill()
        {
            RemoveHealth(_currentHealth + 1);
        }

#endif
    }
}
