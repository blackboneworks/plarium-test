﻿using Controllers.Game.Units.Abilities.Attack;
using Models.Units.Behaviour;
using UnityEngine;

namespace Controllers.Game.Units.Behaviour
{
    public class BehaviourController : MonoBehaviour
    {
        private UnitBehaviour _behaviour;
        
        public bool IsMoving { get; internal set; }
        public IAttackable CurrentTarget { get; internal set; }

        public void Initialize(UnitController unit, UnitBehaviour behaviour)
        {
            if (behaviour == null) return;

            _behaviour = ScriptableObject.Instantiate(behaviour);
            _behaviour.Initialize(unit);
        }

        public void OnEnemySpawned(UnitController enemyUnit)
        {
            if (_behaviour == null) return;
        }

        public void SetMoveTarget(Vector3 worldPosition)
        {
            if (_behaviour == null) return;
            _behaviour.SetMoveTarget(worldPosition);
        }

        public void SetAttackTarget(IAttackable attackable)
        {
            if (_behaviour == null) return;
            _behaviour.SetAttackTarget(attackable);
        }

        private void Update()
        {
            if (_behaviour == null) return;
            if ((Component) CurrentTarget == null) CurrentTarget = null;
            _behaviour.Update();
        }
    }
}