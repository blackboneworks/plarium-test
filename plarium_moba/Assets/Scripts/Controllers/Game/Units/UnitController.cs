﻿using Controllers.Game.Units.Buffs;
using Controllers.Game.Units.HealthSystem;
using Controllers.Selection;

using Models.Parameters;
using Models.Units;

using System;
using System.Collections.Generic;
using System.Linq;
using Controllers.Game.Teams;
using Controllers.Game.Units.Abilities;
using Controllers.Game.Units.Abilities.Attack;
using Controllers.Game.Units.Behaviour;
using Extensions;
using UnityEngine;

namespace Controllers.Game.Units
{
    public class UnitController : MonoBehaviour, ISelectionHandler, IBuffable, IHealthOwner, IAttackable
    {
        // basic
        protected UnitConfig _config;

        private NavMeshAgent _navigationAgent;
        private BuffHub _buffHub;
        private HealthHub _healthHub;
        private AbilitiesHub _abilitiesHub;
        private BehaviourController _behaviourController;

        protected List<IUnitView> _views;
        protected IDictionary<UnitParameter, float> _runtimeParameters;

        // events
        public event Action<UnitController> Died = delegate { };

        // properties
        public UnitConfig Config { get { return _config; } }
        public NavMeshAgent NavigationAgent { get { return _navigationAgent; } }
        public BehaviourController BehaviourController { get { return _behaviourController; } }
        public AbilitiesHub AbilitiesHub { get { return _abilitiesHub; } }
        public TeamBelonging TeamControl { get { return GetComponent<TeamBelonging>(); } }
        public bool IsHero { get { return _config is HeroConfig; } }

        public void Initialize(UnitConfig config)
        {
            // parameters set up
            _config = config;
            _runtimeParameters = _config.Parameters
                .Select(x => new UnitParameterAmount { Parameter = x.Parameter, Amount = x.Amount })
                .ToDictionary(x => x.Parameter, x => x.Amount);

            // initialize navmesh behaviour
            _navigationAgent = gameObject.AddComponent<NavMeshAgent>();
            _navigationAgent.speed = _config.Get(UnitParameter.Speed);
            _navigationAgent.acceleration = 1000;
            _navigationAgent.angularSpeed = 1000;
            _navigationAgent.autoBraking = false;

            // add buff hub
            _buffHub = gameObject.AddComponent<BuffHub>();
            _buffHub.Initialize(this);

            // add health hub
            _healthHub = gameObject.AddComponent<HealthHub>();
            _healthHub.Initialize(this);
            
            // add abilities hub
            _abilitiesHub = gameObject.AddComponent<AbilitiesHub>();
            _abilitiesHub.Initilialize(this, _config);
            _abilitiesHub.AbilityUsed += OnAbilityUsed;

            // add behaviour controller
            _behaviourController = gameObject.AddComponent<BehaviourController>();
            _behaviourController.Initialize(this, _config.Behaviour);

            // collect dependent views
            var views = GetComponentsInChildren(typeof(IUnitView), true);
            _views = views != null && views.Length > 0
                ? new List<IUnitView>(views.Select(x => x as IUnitView))
                : new List<IUnitView>();
            _views.ForEach(x => x.Initialize(this));
        }

        public virtual float GetParameter(UnitParameter parameter)
        {
            if (!_runtimeParameters.ContainsKey(parameter)) return 0f;
            if (BuffHub != null)
                return BuffHub.GetModifiedParameter(parameter, _runtimeParameters[parameter]);
            return _runtimeParameters[parameter];
        }

        #region Base Events Handling
        
        private void OnAbilityUsed(int abilityIndex)
        {
            _views.ForEach(x => x.AbilityUsed(abilityIndex));
        }
        
        #endregion
        
        private void Update()
        {
            // todo make event based
            _views.ForEach(x => x.SetMoving(BehaviourController.IsMoving));
        }

        #region ISelectable Impl

        public void OnSelected()
        {
            EventManager.Input.ActionQueried.OnEvent += OnActionQueried;
            _views.ForEach(x => x.OnSelected());
            HighlightTarget(true);
        }

        public void OnDeselected()
        {
            EventManager.Input.ActionQueried.OnEvent -= OnActionQueried;
            _views.ForEach(x => x.OnDeselected());
            HighlightTarget(false);
        }

        private void OnActionQueried(RaycastHit hit)
        {
            var attackable = hit.collider.GetComponent(typeof(IAttackable)) as IAttackable;
            if (attackable != null)
            { 
                if(!attackable.CanBeAttackedBy(this)) return;
                
                HighlightTarget(false);

                _behaviourController.SetAttackTarget(attackable);
                HighlightTarget(true);

                return;
            }

            _behaviourController.SetMoveTarget(hit.point);
        }

        private void HighlightTarget(bool highlight)
        {
            if (BehaviourController.CurrentTarget != null)
                BehaviourController.CurrentTarget.TargetHighlight(highlight);
        }

        #endregion ISelectable Impl

        #region IBuffable Impl

        public BuffHub BuffHub { get { return _buffHub; } }
        public IDictionary<UnitParameter, float> GetParameters()
        {
            return _runtimeParameters;
        }

        #endregion  IBuffable Impl

        #region IHealthOwner Impl

        public HealthHub HealthHub { get { return _healthHub; } }
        public float BaseHealth { get { return GetParameter(UnitParameter.HP); } }
        
        #endregion IHealthOwner Impl

        #region IAttackable Impl

        public bool CanBeAttackedBy(UnitController unit)
        {
            if (!HealthHub.IsAlive) return false;
            if (unit == null) return false;

            if(TeamControl == null) return true;
            if(unit.TeamControl == null) return true;

            return unit.TeamControl.Team != TeamControl.Team;
        }

        public void Attacked(AttackData attackData)
        {
            // check for inconsistent call
            if (!CanBeAttackedBy(attackData.Attacker)) return;

            HealthHub.RemoveHealth(attackData.Damage);
            
            // check if killed
            if (HealthHub.Health <= 0f)
            {
                HighlightTarget(false);
                EventManager.Units.UnitKilled.Invoke(this, attackData);
                Died.Invoke(this);
                Destroy(gameObject);
            }
        }
        
        public void TargetHighlight(bool enabled)
        {
            _views.ForEach(x => x.TargetHighlight(enabled));
        }

        #endregion

        #region Debugging
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            // draw unit base
            UnityEditor.Handles.color = Color.green;
            UnityEditor.Handles.DrawLine(transform.position, transform.position + transform.forward * 0.75f);
            UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.up, 0.75f);

            if (_config == null) return;

            // draw unit range
            UnityEditor.Handles.color = Color.yellow;
            UnityEditor.Handles.DrawLine(transform.position, transform.position + transform.forward * _config.Get(UnitParameter.AttackRange));
            UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.up, _config.Get(UnitParameter.AttackRange));
        }
#endif
        #endregion
    }
}