﻿using System;
using System.Linq;
using Extensions;
using Models.Parameters;
using Models.Units;
using Models.Units.Abilities;
using Models.Units.Abilities.Interfaces;
using UnityEngine;

namespace Controllers.Game.Units.Abilities
{
    public class AbilitiesHub : MonoBehaviour
    {
        [SerializeField] private UnitAbility[] _abilities;

        private UnitController _unit;
        private IPreparationModeAbility _executingAbility;
        private bool _nextUseIsIncrease;

        public int AbilitiesCount { get { return _abilities.Length; } }

        public event Action<int> AbilityUsed = delegate { };

        public void Initilialize(UnitController unit, UnitConfig config)
        {
            _unit = unit;

            _abilities = new UnitAbility[config.Abilities.Length];
            for (int i = 0; i < config.Abilities.Length; i++)
                _abilities[i] = ScriptableObject.Instantiate(config.Abilities[i]);

            EventManager.Progress.HeroLevelChanged.OnEvent += OnHeroLevelUp;
            unit.Died += OnUnitDied;
        }

        private void OnUnitDied(UnitController unit)
        {
            if(_executingAbility != null) _executingAbility.Denied();
        }

        private void OnDestroy()
        {
            EventManager.Progress.HeroLevelChanged.OnEvent -= OnHeroLevelUp;
        }

        private void OnHeroLevelUp(int level)
        {
            if (_abilities.OfType<ILeveledAbility>().Any(x => x.CanBeUpgraded))
                    _nextUseIsIncrease = true;
        }

        public UnitAbility GetAbility(int index)
        {
            if (_abilities == null) return null;
            if (_abilities.Length == 0) return null;
            if (index >= _abilities.Length) return null;

            return _abilities[index];
        }

        public void UseAbility(int abilityIndex = 0, GameObject target = null)
        {
            if (_nextUseIsIncrease)
            {
                if(!GetAbility(abilityIndex) is ILeveledAbility) return;
                if(!(GetAbility(abilityIndex) as ILeveledAbility).CanBeUpgraded) return;

                (GetAbility(abilityIndex) as ILeveledAbility).LevelUp();
                _nextUseIsIncrease = false;
            }

            if (!CanUseAbility(abilityIndex)) return;

            if (_abilities[abilityIndex] is IPreparationModeAbility)
            {
                _abilities[abilityIndex].ExecuteAbility(_unit, target, used =>
                {
                    if (used)
                    {
                        _abilities[abilityIndex].LastUsedTime = Time.time;
                        AbilityUsed.Invoke(abilityIndex);
                    }
                    _executingAbility = null;
                });
                _executingAbility = _abilities[abilityIndex] as IPreparationModeAbility;
            }
            else
                _abilities[abilityIndex].ExecuteAbility(_unit, target, used =>
                {
                    if (used)
                    {
                        _abilities[abilityIndex].LastUsedTime = Time.time;
                        AbilityUsed.Invoke(abilityIndex);
                    }
                });
        }

        public bool CanUseAbility(int abilityIndex = 0)
        {
            if (_abilities == null) return false;
            if (_abilities.Length == 0) return false;
            if (abilityIndex >=_abilities.Length) return false;
            if (_executingAbility != null) return false;

            return _abilities[abilityIndex].LastUsedTime + GetAblilityCooldown(abilityIndex) <= Time.time;
        }

        public float GetAblilityAttackRange(int abilityIndex = 0)
        {
            if (_abilities == null) return -1;
            if (_abilities.Length == 0) return -1;
            if (abilityIndex >= _abilities.Length) return -1;

            if (_abilities[abilityIndex] is ICustomRangeAbility)
                return (_abilities[abilityIndex] as ICustomRangeAbility).Range;

            return _unit.GetParameter(UnitParameter.AttackRange);
        }

        public float GetAblilityCooldown(int abilityIndex = 0)
        {
            if (_abilities == null) return -1;
            if (_abilities.Length == 0) return -1;
            if (abilityIndex >= _abilities.Length) return -1;

            if (_abilities[abilityIndex] is ICooldownAbility)
                return (_abilities[abilityIndex] as ICooldownAbility).Cooldown;

            return 1 / _unit.GetParameter(UnitParameter.AttackSpeed);
        }

        private void Update()
        {
            if(_executingAbility != null)
                _executingAbility.Update();
        }
    }

    [Serializable]
    public class AbilityState
    {
        public UnitAbility Ability;
        public float LastUsedTime;
    }
}