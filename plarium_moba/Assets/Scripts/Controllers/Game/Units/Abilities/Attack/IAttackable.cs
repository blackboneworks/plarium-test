﻿using UnityEngine;

namespace Controllers.Game.Units.Abilities.Attack
{
    public interface IAttackable
    {
        Transform transform { get; }
        GameObject gameObject { get; }

        bool CanBeAttackedBy(UnitController unit);
        void Attacked(AttackData attackData);
        void TargetHighlight(bool enabled);
    }
}