﻿namespace Controllers.Game.Units.Abilities.Attack
{
    public class AttackData
    {
        public UnitController Attacker;
        public float Damage;
        // TODO other attack necessery data

        public AttackData(UnitController attacker, float damage)
        {
            Attacker = attacker;
            Damage = damage;
        }
    }
}