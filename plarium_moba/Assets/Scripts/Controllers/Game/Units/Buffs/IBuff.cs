﻿namespace Controllers.Game.Units.Buffs
{
    public interface IBuff
    {
        void OnBuffAdded(IBuffable unit);
        void OnBuffRemoved(IBuffable unit);
    }
}