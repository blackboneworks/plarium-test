﻿using System.Collections.Generic;
using Models.Parameters;

namespace Controllers.Game.Units.Buffs
{
    public interface IBuffable
    {
        BuffHub BuffHub { get; }
        IDictionary<UnitParameter, float> GetParameters();
    }
}