﻿namespace Controllers.Game.Units.Buffs
{
    public interface IBuffUpdatable : IBuff
    {
        void Update(IBuffable unit);
    }
}