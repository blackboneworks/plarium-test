﻿using System.Collections.Generic;
using Models.Parameters;

namespace Controllers.Game.Units.Buffs
{
    public abstract class ParameterModifierBuff : IBuff
    {
        public IDictionary<UnitParameter, float> ParameterUpgrades
        {
            get { return _parametersUpgrades; }
        }

        private readonly IDictionary<UnitParameter, float> _parametersUpgrades;

        protected ParameterModifierBuff(IDictionary<UnitParameter, float> parameters)
        {
            _parametersUpgrades = parameters;
        }

        public virtual void OnBuffAdded(IBuffable unit)
        {
            foreach (var upgrade in _parametersUpgrades)
            {
                if (unit.GetParameters().ContainsKey(upgrade.Key))
                    unit.GetParameters()[upgrade.Key] += upgrade.Value;
            }
        }
        
        public virtual void OnBuffRemoved(IBuffable unit)
        {
            foreach (var upgrade in _parametersUpgrades)
            {
                if (unit.GetParameters().ContainsKey(upgrade.Key))
                    unit.GetParameters()[upgrade.Key] -= upgrade.Value;
            }
        }
    }
}