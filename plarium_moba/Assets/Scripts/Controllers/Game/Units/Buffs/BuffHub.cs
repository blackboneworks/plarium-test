﻿using System;
using System.Collections.Generic;
using Models.Parameters;
using UnityEngine;

namespace Controllers.Game.Units.Buffs
{
    public class BuffHub : MonoBehaviour
    {
        public IBuffable Target { get; private set; }

        public int BuffCount { get { return _buffs.Count; } }
        public IEnumerable<IBuff> Buffs { get { return _buffs.Values; } }

        private readonly Dictionary<Type, IBuff> _buffs = new Dictionary<Type, IBuff>();
        private readonly List<IBuffUpdatable> _updatableBuffs = new List<IBuffUpdatable>();
        
        public void Initialize(IBuffable target)
        {
            Target = target;
        }

        public void AddBuff(IBuff buff)
        {
            var buffType = buff.GetType();

            RemoveBuff(buff);

            _buffs.Add(buffType, buff);

            if(buff is IBuffUpdatable)
                _updatableBuffs.Add(buff as IBuffUpdatable);

            buff.OnBuffAdded(Target);
        }

        public void RemoveBuff(Type buffType)
        {
            if (!_buffs.ContainsKey(buffType)) return;

            _updatableBuffs.RemoveAll(x => x.GetType() == buffType);

            _buffs[buffType].OnBuffRemoved(Target);
            _buffs.Remove(buffType);
        }

        public void RemoveBuff(IBuff buff)
        {
            var buffType = buff.GetType();
            if (!_buffs.ContainsKey(buffType)) return;

            if (buff is IBuffUpdatable)
                _updatableBuffs.Remove(buff as IBuffUpdatable);

            _buffs[buffType].OnBuffRemoved(Target);
            _buffs.Remove(buffType);
        }

        public float GetModifiedParameter(UnitParameter parameter, float amount)
        {
            // TODO check current buffs
            return amount;
        }

        private void Update()
        {
            for (int i = 0; i < _updatableBuffs.Count; i++)
                _updatableBuffs[i].Update(Target);
        }
    }
}