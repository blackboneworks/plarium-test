﻿using Controllers.Game.Teams;
using Controllers.Game.Units;
using Controllers.Game.Units.Abilities.Attack;
using Controllers.Game.Units.HealthSystem;
using Controllers.Selection;
using Extensions;
using Models.Buildings;
using UnityEngine;
using View.Buildings;

namespace Controllers.Game.Buldings
{
    public class SofaController : MonoBehaviour, IAttackable, IHealthOwner, ISelectionHandler
    {
        [SerializeField] private SofaConfig _config;
        [SerializeField] private SofaView _view;

        private HealthHub _healthHub;

        public SofaConfig Config { get { return _config; } }
        public TeamBelonging TeamControl { get { return GetComponent<TeamBelonging>(); } }

        private void Awake()
        {
            EventManager.Game.GameInitialized.OnEvent += OnGameInitialzied;
        }

        private void OnDestroy()
        {
            EventManager.Game.GameInitialized.OnEvent -= OnGameInitialzied;
        }

        public void OnGameInitialzied(GameSettings gameSettings)
        {
            _healthHub = gameObject.AddComponent<HealthHub>();
            _healthHub.Initialize(this);

            gameObject.AddComponent<Selectable>();
            _view.Initialzie(this);
            
            Debug.Log("<color=green>[Sofa]</color> Initialized.");
        }

        #region IHealthOwner Impl
        
        public HealthHub HealthHub
        {
            get
            {
                return _healthHub;
            }
        }

        public float BaseHealth
        {
            get
            {
                return _config.HP;
            }
        }

        #endregion IHealthOwner Impl

        #region IAttackable Impl

        public bool CanBeAttackedBy(UnitController unit)
        {
            if (!HealthHub.IsAlive) return false;

            if(TeamControl == null) return true;
            if(unit.TeamControl == null) return true;

            return unit.TeamControl.Team != TeamControl.Team;
        }

        public void Attacked(AttackData attackData)
        {
            if (!CanBeAttackedBy(attackData.Attacker)) return;

            HealthHub.RemoveHealth(attackData.Damage);

            if (!HealthHub.IsAlive)
                EventManager.Game.PlayerBaseDestroyed.Invoke();
        }

        public void TargetHighlight(bool enabled)
        {
            _view.Highlight(enabled);
        }

        public void OnSelected()
        {
        }

        public void OnDeselected()
        {
        }

        #endregion IAttakcable Impl
    }
}
