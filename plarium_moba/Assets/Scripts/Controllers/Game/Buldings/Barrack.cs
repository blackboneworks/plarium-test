﻿using System.Collections;
using System.Collections.Generic;
using Controllers.Game.Buldings.Buffs;
using Controllers.Game.Buldings.Production;
using Controllers.Game.Teams;
using Controllers.Game.Units;
using Controllers.Selection;
using Extensions;
using Models.Buildings;
using Models.Parameters;
using UnityEngine;

namespace Controllers.Game.Buldings
{
    public class Barrack : UnitBuilder, ISelectionHandler
    {
        [SerializeField] private BarrackConfig _config;

        private Coroutine _minionSpawningCoroutine;
        private List<UnitController> _units;
        private GameSettings _gameSettings;

        public int Level { get; private set; }
        public int NextLevelPrice
        {
            get
            {
                if (Level == _config.LevelConfigs.Length) return -1;
                return _config.LevelConfigs[Level].Price;
            }
        }

        public float CurrentUnitProgress { get; private set; }
        public float TrainingTime
        {
            get
            {
                return _config.GetLeveledParameter(BuildingParameter.TrainingSpeed, Level);
            }
        }
        public BarrackConfig Config { get { return _config; } }
        public TeamBelonging TeamControl { get { return GetComponent<TeamBelonging>(); } }

        private void Awake()
        {
            // subscribe for necessery game events
            EventManager.Game.GameInitialized.OnEvent += OnGameInitialized;
        }
        
        private void OnDestroy()
        {
            // unsubscribe for necessery game events
            EventManager.Game.GameInitialized.OnEvent -= OnGameInitialized;
        }

        private void OnGameInitialized(GameSettings gameSettings)
        {
            EventManager.Game.PreparationEnded.OnEvent += OnPreparationEnded;
            _gameSettings = gameSettings;
            Level = 1;
            _units = new List<UnitController>();
            gameObject.AddComponent<Selectable>();

            Debug.Log("<color=green>[Barrack]</color> Initialized.");
        }

        private void OnPreparationEnded()
        {
            EventManager.Game.PreparationEnded.OnEvent -= OnPreparationEnded;

            if (_minionSpawningCoroutine != null)
                StopCoroutine(_minionSpawningCoroutine);
            _minionSpawningCoroutine = StartCoroutine(MinionSpawningRoutine());

            Debug.Log("<color=green>[Barrack]</color> Minions production started.");
        }

        public bool LevelUp()
        {
            if (Level == _config.LevelConfigs.Length) return false;
            if(_gameSettings.HeroProgress.Gold < NextLevelPrice) return false;

            _gameSettings.HeroProgress.Gold -= NextLevelPrice;
            Level++;
            foreach (var unit in _units)
                unit.BuffHub.AddBuff(new BarrackBuff(GetParameterUpgrades()));

            return true;
        }

        private IEnumerator MinionSpawningRoutine()
        {
            while (true)
            {
                var startTime = Time.time;
                while (Time.time < startTime + TrainingTime)
                {
                    yield return new WaitForEndOfFrame();
                    CurrentUnitProgress = (Time.time - startTime) / TrainingTime;
                }
                
                var unit = CreaterUnit(_config.UnitConfig, mUnit =>
                {
                    if (TeamControl != null)
                        mUnit.TeamControl.Team = TeamControl.Team;

                }, typeof(GroupSelectable), typeof(TeamBelonging));
                
                _units.Add(unit);
                unit.Died += OnUnitDied;

                var parameters = GetParameterUpgrades();
                unit.BuffHub.AddBuff(new BarrackBuff(parameters));
                // hix HP diff
                if(parameters.ContainsKey(UnitParameter.HP))
                    unit.HealthHub.AddHealth(parameters[UnitParameter.HP]);

                Debug.Log("<color=green>[Barrack]</color> Minion spawned.");

                EventManager.Units.UnitSpawned.Invoke(unit);
                EventManager.Units.AllyUnitSpawned.Invoke(unit);
            }
        }

        private void OnUnitDied(UnitController unit)
        {
            _units.Remove(unit);
        }

        private IDictionary<UnitParameter, float> GetParameterUpgrades()
        {
            var dict = new Dictionary<UnitParameter, float>();

            for (int l = 0; l < Level; l++)
            {
                foreach (var unitParametersChange in _config.LevelConfigs[l].UnitParametersChanges)
                {
                    if (dict.ContainsKey(unitParametersChange.Parameter))
                        dict[unitParametersChange.Parameter] += unitParametersChange.Amount;
                    else
                        dict.Add(unitParametersChange.Parameter, unitParametersChange.Amount);
                }
            }

            return dict;
        }

        public void OnSelected()
        {
        }

        public void OnDeselected()
        {
        }
    }
}