﻿using System;
using System.Linq;
using Controllers.Game.Buldings.Buffs;
using Controllers.Game.Teams;
using Controllers.Game.Units;
using Controllers.Game.Units.Buffs;
using Extensions;
using Models.Buildings;
using UnityEngine;

namespace Controllers.Game.Buldings.Buffers
{
    public class FountainBuffArea : MonoBehaviour
    {
        [SerializeField] private ColliderNotifier AreaNotifier;
        private UnitRegenerationAmount[] _buffAreaBindings;

        public TeamBelonging TeamControl { get { return GetComponent<TeamBelonging>(); } }

        public void Initialize(UnitRegenerationAmount[] buffAreaeSettings)
        {
            _buffAreaBindings = buffAreaeSettings;

            AreaNotifier.TriggerEnterEvent += OnUnitTriggerEnter;
            AreaNotifier.TriggerExitEvent += OnUnitTriggerExit;
        }

        private void OnUnitTriggerEnter(Collider other)
        {
            var buffable = other.GetComponent<IBuffable>();
            if(buffable == null) return;
            var unit = buffable as UnitController;
            if (unit == null) return;
            if (unit.TeamControl.Team != TeamControl.Team) return;
            var binding = _buffAreaBindings.FirstOrDefault(x => x.Unit == unit.Config);
            if(binding == null) return;
            unit.BuffHub.AddBuff(new RegenerationBuff(binding.Amount, binding.Period));
        }

        private void OnUnitTriggerExit(Collider other)
        {
            var buffable = other.GetComponent<IBuffable>();
            if (buffable == null) return;
            var unit = buffable as UnitController;
            if (unit == null) return;
            if (unit.TeamControl.Team != TeamControl.Team) return;
            var binding = _buffAreaBindings.FirstOrDefault(x => x.Unit == unit.Config);
            if (binding == null) return;
            unit.BuffHub.RemoveBuff(typeof(RegenerationBuff));
        }
    }
}