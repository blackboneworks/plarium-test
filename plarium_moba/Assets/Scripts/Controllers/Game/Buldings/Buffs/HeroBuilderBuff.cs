﻿using System.Collections.Generic;
using Controllers.Game.Units.Buffs;
using Models.Parameters;

namespace Controllers.Game.Buldings.Buffs
{
    public class HeroBuilderBuff : ParameterModifierBuff
    {
        public HeroBuilderBuff(IDictionary<UnitParameter, float> parameters) : base(parameters)
        {
        }
    }
}