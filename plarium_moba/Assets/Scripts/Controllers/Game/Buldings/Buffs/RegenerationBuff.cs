﻿using Controllers.Game.Units;
using Controllers.Game.Units.Buffs;
using UnityEngine;

namespace Controllers.Game.Buldings.Buffs
{
    public class RegenerationBuff : IBuffUpdatable
    {
        private float _amount;
        private float _peroind;
        private float _lastTime;

        public RegenerationBuff(float amount, float period)
        {
            _amount = amount;
            _peroind = period;
        }

        public void OnBuffAdded(IBuffable unit)
        {
            // TODO : add view
        }
        public void OnBuffRemoved(IBuffable unit)
        {
            // TODO : remove view
        }
        
        public void Update(IBuffable unit)
        {
            var controller = unit as UnitController;
            if(controller == null) return;
            
            if(_lastTime + _peroind > Time.time) return;
            
            controller.HealthHub.AddHealth(_amount);
            _lastTime = Time.time;
        }
    }
}