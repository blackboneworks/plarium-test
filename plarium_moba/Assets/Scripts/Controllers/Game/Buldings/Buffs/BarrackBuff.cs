﻿using System.Collections.Generic;
using Controllers.Game.Units.Buffs;
using Models.Parameters;

namespace Controllers.Game.Buldings.Buffs
{
    // override base logic make different parameter affectors more concrete
    public class BarrackBuff : ParameterModifierBuff
    {
        public BarrackBuff(IDictionary<UnitParameter, float> parameters) : base(parameters)
        {
        }
    }
}