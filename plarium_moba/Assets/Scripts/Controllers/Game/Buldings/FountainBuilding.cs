﻿using Controllers.Game.Buldings.Buffers;
using Controllers.Game.Buldings.Production;
using Controllers.Game.Units;
using Controllers.Game.Units.Abilities.Attack;
using Extensions;
using Models.Buildings;
using UnityEngine;

namespace Controllers.Game.Buldings
{
    public class FountainBuilding : MonoBehaviour
    {
        [Header("Config")]
        [SerializeField] private FountainConfig _config;

        [Header("References")]
        [SerializeField] private HeroBuilder _builder;
        [SerializeField] private FountainBuffArea _buffArea;

        private GameSettings _gameSettings;

        private void Awake()
        {
            // subscribe for necessery game events
            EventManager.Game.GameInitialized.OnEvent += OnGameInitialized;
            EventManager.Hero.HeroKilled.OnEvent += OnHeroKilled;
            EventManager.Progress.HeroLevelChanged.OnEvent += OnHeroLevelChanged;
        }

        private void OnDestroy()
        {
            // unsubscribe for necessery game events
            EventManager.Game.GameInitialized.OnEvent -= OnGameInitialized;
            EventManager.Hero.HeroKilled.OnEvent -= OnHeroKilled;
            EventManager.Progress.HeroLevelChanged.OnEvent -= OnHeroLevelChanged;
        }


        private void OnGameInitialized(GameSettings settings)
        {
            // save reference to global game settings
            _gameSettings = settings;

            // spawn hero immediately
            _builder.Initialize(_config.Unit);
            _builder.SpawnHero(_gameSettings.HeroProgress.HeroLevel);

            // enable buff area
            _buffArea.Initialize(_config.BuffAreaBindings);

            Debug.Log("<color=green>[Fountain]</color> Initialized.");
        }
        
        private void OnHeroKilled(UnitController hero, AttackData killer)
        {
            var respawnTime = _config.Unit.Levels[_gameSettings.HeroProgress.HeroLevel].RespawnTime;
            EventManager.Hero.HeroRespawnCountdownStarted.Invoke(respawnTime);
            this.Invoke(() => _builder.SpawnHero(_gameSettings.HeroProgress.HeroLevel), respawnTime);
        }

        private void OnHeroLevelChanged(int level)
        {
            _builder.CheckoutHero(level);
        }
    }
}