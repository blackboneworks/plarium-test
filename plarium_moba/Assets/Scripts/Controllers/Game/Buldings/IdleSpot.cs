﻿using Controllers.Game.Teams;
using UnityEngine;

namespace Controllers.Game.Buldings
{
    [RequireComponent(typeof(TeamBelonging))]
    public class IdleSpot : MonoBehaviour
    {
        [SerializeField] private Transform _point;

        public TeamBelonging TeamControl { get { return GetComponent<TeamBelonging>(); } }
        public Vector3 GetPoint()
        {
            return _point.position;
        }
    }
}
