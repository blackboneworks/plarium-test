﻿using System;
using Controllers.Game.Units;
using Models.Units;
using UnityEngine;

namespace Controllers.Game.Buldings.Production
{
    /// <summary>
    /// Builder compontent to create units.
    /// </summary>
    public class UnitBuilder : MonoBehaviour
    {
        [SerializeField] private Transform[] _spawnPoints;
        
        private int _currentIndex;
        
        public event Action<UnitController> UnitSpawned = delegate { };
        
        public virtual UnitController CreaterUnit(UnitConfig config, Action<UnitController> beforeEvent = null, params Type[] additionalComponents)
        {
            if (config == null) throw new Exception("Builder not initialized!");

            var spawnPoint = NextSpawnPoint();
            var unit = Instantiate(config.Prefab);
            unit.transform.position = spawnPoint.position;
            unit.transform.rotation = spawnPoint.rotation;

            var controller = unit.AddComponent<UnitController>();

            for (int i = 0; i < additionalComponents.Length; i++)
                unit.AddComponent(additionalComponents[i]);

            if (beforeEvent != null)
                beforeEvent.Invoke(controller);

            controller.Initialize(config);

            UnitSpawned.Invoke(controller);

            return controller;
        }

        protected Transform NextSpawnPoint()
        {
            if (_currentIndex == _spawnPoints.Length) _currentIndex = 0;
            return _spawnPoints[_currentIndex++];
        }
    }
}