﻿using System.Collections.Generic;
using Controllers.Game.Buldings.Buffs;
using Controllers.Game.Teams;
using Controllers.Game.Units;
using Controllers.Selection;
using Models.Units;
using Extensions;
using Models.Parameters;

namespace Controllers.Game.Buldings.Production
{
    public class HeroBuilder : UnitBuilder
    {
        private HeroConfig _config;
        private UnitController _hero;

        public HeroConfig HeroConfig { get { return _config; } }
        public TeamBelonging TeamControl { get { return GetComponent<TeamBelonging>(); } }

        public void Initialize(HeroConfig config)
        {
            _config = config;
        }

        public void CheckoutHero(int level)
        {
            if(_hero == null) return;
            Instantiate(_config.LevelUpEffect, _hero.transform.position, _hero.transform.rotation);
            _hero.BuffHub.AddBuff(new HeroBuilderBuff(GetParameterUpgrades(level)));
        }

        public void SpawnHero(int heroLevel)
        {
            // spawn unit
            _hero = CreaterUnit(_config, mUnit =>
            {
                if (TeamControl != null)
                    mUnit.TeamControl.Team = TeamControl.Team;

            }, typeof(GroupSelectable), typeof(TeamBelonging));

            // add level increasions
            _hero.BuffHub.AddBuff(new HeroBuilderBuff(GetParameterUpgrades(heroLevel)));

            // add team
            if (TeamControl != null)
                _hero.TeamControl.Team = TeamControl.Team;
            
            // fire event
            EventManager.Hero.HeroSpawned.Invoke(_hero);
        }
        
        private IDictionary<UnitParameter, float> GetParameterUpgrades(int heroLevel)
        {
            var dict = new Dictionary<UnitParameter, float>();

            for (int l = 0; l < heroLevel; l++)
            {
                foreach (var unitParametersChange in _config.Levels[l].Adjusts)
                {
                    if (dict.ContainsKey(unitParametersChange.Parameter))
                        dict[unitParametersChange.Parameter] += unitParametersChange.Amount;
                    else
                        dict.Add(unitParametersChange.Parameter, unitParametersChange.Amount);
                }
            }

            return dict;
        }
    }
}