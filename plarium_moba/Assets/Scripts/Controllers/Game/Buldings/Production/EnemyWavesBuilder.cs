﻿using System;
using System.Collections;
using System.Collections.Generic;
using Controllers.Game.Teams;
using Controllers.Game.Units;
using Controllers.Selection;
using Extensions;
using Models.Buildings;
using UnityEngine;

namespace Controllers.Game.Buldings.Production
{
    public class EnemyWavesBuilder : UnitBuilder
    {
        [SerializeField] private EnemiesSpawnerConfig _config;

        private List<UnitController> _units;
        private Queue<EnemiesWave> _waves;
        private int _currentWaveNumber;

        public TeamBelonging TeamControl { get { return GetComponent<TeamBelonging>(); } }

        private void Awake()
        {
            EventManager.Game.GameInitialized.OnEvent += OnGameInitialzied;
        }

        private void OnDestroy()
        {
            EventManager.Game.GameInitialized.OnEvent -= OnGameInitialzied;
        }

        private void OnGameInitialzied(GameSettings gameSettings)
        {
            EventManager.Game.PreparationEnded.OnEvent += OnPreparationEnded;
            Debug.Log("<color=green>[EnemyBuilder]</color> Initialized.");
        }

        private void OnPreparationEnded()
        {
            EventManager.Game.PreparationEnded.OnEvent -= OnPreparationEnded;

            _units = new List<UnitController>();
            _waves = new Queue<EnemiesWave>(_config.Waves);
            _currentWaveNumber = 1;
            
            SpawnNextWave();
        }
        
        public void SpawnNextWave()
        {
            var wave = _waves.Dequeue();
            StartCoroutine(WaveRoutine(wave));
            
            var remaining = wave.Delay;
            while (remaining > 0)
            {
                var rem = remaining;
                this.Invoke(() => Debug.Log("<color=green>[EnemyBuilder]</color> " + rem + " seconds to next wave."), wave.Delay - rem);
                remaining--;
            }
        }

        private IEnumerator WaveRoutine(EnemiesWave wave)
        {
            Debug.Log("<color=green>[EnemyBuilder]</color> Spawning enemies...");

            EventManager.Game.EnemyWaveCountdownStarted.Invoke(wave.Delay);

            yield return new WaitForSeconds(wave.Delay);

            EventManager.Game.EnemyWaveStarted.Invoke(_currentWaveNumber);

            foreach (var units in wave.Units)
            {
                for (int i = 0; i < units.Amount; i++)
                {
                    var unit = CreaterUnit(units.Unit, mUnit =>
                    {
                        if (TeamControl != null)
                            mUnit.TeamControl.Team = TeamControl.Team;

                    }, typeof(Selectable), typeof(TeamBelonging));

                    unit.Died += OnUnitDied;
                    _units.Add(unit);
                    yield return new WaitForSeconds(wave.PerUnitDelay);
                }
            }
            
            Debug.Log("<color=green>[EnemyBuilder]</color> Spawning enemies completed. Waiting to kill them.");

            while (_units.Count > 0)
                yield return new WaitForFixedUpdate();

            if (_waves.Count == 0)
            {
                Debug.Log("<color=green>[EnemyBuilder]</color> All enemies killed. No waves ahead. WIN!");

                EventManager.Game.AllEnemyWavesCompleted.Invoke();
                yield break;
            }

            Debug.Log("<color=green>[EnemyBuilder]</color> All enemies killed. Spawning next wave.");

            EventManager.Game.EnemyWaveCompleted.Invoke(_currentWaveNumber);
            _currentWaveNumber++;

            SpawnNextWave();
        }

        private void OnUnitDied(UnitController unit)
        {
            unit.Died -= OnUnitDied;
            _units.Remove(unit);
        }
    }
}