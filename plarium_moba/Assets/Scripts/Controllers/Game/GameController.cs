﻿using System;
using System.Collections.Generic;
using System.Linq;
using Controllers.Cameras;
using Controllers.Game.Buldings;
using Controllers.Game.Buldings.Buffers;
using Controllers.Game.Buldings.Production;
using Controllers.Game.Units;
using Controllers.Game.Units.Abilities.Attack;
using Controllers.Selection;
using Models;
using Extensions;
using Models.Parameters;
using UnityEngine;

namespace Controllers.Game
{
    public class GameController : MonoBehaviour
    {
        [Header("Config")]
        [SerializeField] private GameConfig _config;

        [Header("References")]
        [SerializeField] private PointerController _pointerController;
        [SerializeField] private CameraController _cameraController;

        [Header("Ally Buildings")]
        // Fountain is splited to hero builder and buff area
        [SerializeField] private HeroBuilder _heroBuilder;
        [SerializeField] private FountainBuffArea _fountainBuffArea;

        [SerializeField] private Barrack[] _minionBuilders;
        [SerializeField] private SofaController _sofa;

        [Header("Enemy Buildings")]
        [SerializeField] private EnemyWavesBuilder _enemyWavesBuilder;

        private GameSettings _settings;

        // fields for team things logic
        private Dictionary<Team, List<UnitController>> _units;

        public GameSettings Settings { get { return _settings; } }

        #region Game Start

        private void Awake()
        {
            EventManager.Game.GameStarged.OnEvent += OnGameStarted;
        }

        private void Start()
        {
            // fire start game event
            EventManager.Game.GameStarged.Invoke(_config.SpawnersStartTime);

            // fire end of preparation event
            this.Invoke(EventManager.Game.PreparationEnded.Invoke, _config.SpawnersStartTime);
        }

        private void OnGameStarted(float delay)
        {
            _settings = new GameSettings();

            // initialize user progress
            _settings.HeroProgress = new HeroProgress(_config.StartGold);

            var remaining = delay;
            while (remaining > 0)
            {
                var rem = remaining;
                this.Invoke(() => Debug.Log("<color=green>[GameController]</color> " + rem + " seconds remaining."), delay - rem);
                remaining--;
            }

            EventManager.Units.UnitKilled.OnEvent += OnUnitKilled;

            EventManager.Game.AllEnemyWavesCompleted.OnEvent += OnAllWavesCompleted;
            EventManager.Game.PlayerBaseDestroyed.OnEvent += OnPlayerBaseDestroyed;
            EventManager.Game.GameEnded.OnEvent += OnGameEnded;

            // fire game initialized event
            // it is start point for all dependent things
            EventManager.Game.GameInitialized.Invoke(_settings);
        }

        private void OnGameEnded(bool win)
        {
            _heroBuilder.enabled = false;
            _fountainBuffArea.enabled = false;
            foreach (var minionBuilder in _minionBuilders)
                minionBuilder.enabled = false;
            _sofa.enabled = false;
            _cameraController.enabled = false;
            _pointerController.enabled = false;

            EventManager.Flush();
        }

        private void OnPlayerBaseDestroyed()
        {
            EventManager.Game.GameEnded.Invoke(false);
        }

        private void OnAllWavesCompleted()
        {
            EventManager.Game.GameEnded.Invoke(true);
        }

        private void OnUnitKilled(UnitController unit, AttackData attackData)
        {
            var colora = attackData.Attacker.TeamControl.Team == _config.PlayerTeam ? "green" : "red";
            var colorb = unit.TeamControl.Team == _config.PlayerTeam ? "green" : "red";
            Debug.Log("<color=" + colora + ">[" + attackData.Attacker.name + "]</color> killed <color=" + colorb + ">[" +
                      unit.name + "]</color>");

            if (unit.TeamControl.Team == _config.PlayerTeam)
                EventManager.Units.AllyUnitKilled.Invoke(unit, attackData);
            if (unit.IsHero)
                EventManager.Hero.HeroKilled.Invoke(unit, attackData);
            else
            {
                EventManager.Units.EnemyUnitKilled.Invoke(unit, attackData);

                if (attackData.Attacker.IsHero)
                {
                    _settings.HeroProgress.Gold += (int) unit.GetParameter(UnitParameter.RewardGold);
                    EventManager.Progress.GoldAmountChanged.Invoke(_settings.HeroProgress.Gold);
                }

                var xp = (int) unit.GetParameter(UnitParameter.RewardXP);
                _settings.HeroProgress.XP += xp;

                if (_heroBuilder.HeroConfig.Levels.Length != _settings.HeroProgress.HeroLevel)
                    if (_settings.HeroProgress.XP >=
                        _heroBuilder.HeroConfig.Levels[_settings.HeroProgress.HeroLevel].Experience)
                    {
                        _settings.HeroProgress.XP -=
                            _heroBuilder.HeroConfig.Levels[_settings.HeroProgress.HeroLevel].Experience;
                        _settings.HeroProgress.HeroLevel++;

                        EventManager.Progress.HeroLevelChanged.Invoke(_settings.HeroProgress.HeroLevel);
                    }

                EventManager.Progress.XPChanged.Invoke(_settings.HeroProgress.HeroLevel);
            }
        }

        #endregion
        
        private void OnGUI()
        {
            if(_settings == null) return;
            if(_settings.HeroProgress == null) return;
            
            GUI.BeginGroup(new Rect(10, 60, 200, 400));

            GUILayout.Label("Level : " + _settings.HeroProgress.HeroLevel);
            GUILayout.Label("XP : " + _settings.HeroProgress.XP);
            GUILayout.Label("Gold : " + _settings.HeroProgress.Gold);

            GUI.EndGroup();
        }
    }

    [Serializable]
    public class GameSettings
    {
        public HeroProgress HeroProgress;
    }

    [Serializable]
    public class HeroProgress
    {
        public bool GameStarted;
        public int Gold;
        public int XP;
        public int HeroLevel;

        public HeroProgress(int gold, int xp = 0)
        {
            Gold = gold;
            XP = xp;
            GameStarted = true;
            HeroLevel = 1;
        }
    }
}