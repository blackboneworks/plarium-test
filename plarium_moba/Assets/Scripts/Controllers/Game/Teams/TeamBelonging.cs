﻿using Models;
using UnityEngine;

namespace Controllers.Game.Teams
{
    public class TeamBelonging : MonoBehaviour
    {
        [SerializeField] private Team _team;
        public Team Team
        {
            get { return _team; }
            set { _team = value; }
        }
    }
}