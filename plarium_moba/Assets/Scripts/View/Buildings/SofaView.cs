﻿using Controllers.Game.Buldings;
using UnityEngine;
using View.Units;

namespace View.Buildings
{
    public class SofaView : MonoBehaviour
    {
        [SerializeField] private GameObject _targetHighlight;
        [SerializeField] private HPBarWidget _hpBar;

        private SofaController _sofaController;

        public void Initialzie(SofaController sofaController)
        {
            _sofaController = sofaController;
            _hpBar.Hub = _sofaController.HealthHub;
        }

        public void Highlight(bool enabled)
        {
            _targetHighlight.SetActive(enabled);
        }
    }
}
