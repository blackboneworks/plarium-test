﻿using UnityEngine;
using _UI.UIExtensions;

public class RectTransformGrid : GridController<RectTransform>
{
    private RectTransform m_rectTransform;

    public RectTransform rectTransform
    {
        get
        {
            if (m_rectTransform == null)
                m_rectTransform = GetComponent<RectTransform>();
            return m_rectTransform;
        }
    }
}
