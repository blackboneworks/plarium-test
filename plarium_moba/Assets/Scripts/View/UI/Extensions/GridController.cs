﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace _UI.UIExtensions
{
    public abstract class GridController<T> : MonoBehaviour where T : Component
    {
        protected bool m_initialized;

        public T prefab;

        protected virtual void Awake()
        {
            if(m_initialized) return;

            if (prefab == null)
                prefab = GetComponentsInChildren<T>().FirstOrDefault(x => x.gameObject != this.gameObject);

            if (prefab != null)
                prefab.transform.SetParent(null);

            Clear(0, true);

            if (prefab != null)
                prefab.transform.SetParent(transform);

            if (prefab != null)
                prefab.gameObject.SetActive(false);

            m_initialized = true;
        }

        public virtual void Clear(int left = 1, bool @internal = false)
        {
            if(!m_initialized && !@internal) Awake();

            while (transform.childCount > left)
                DestroyImmediate(transform.GetChild(left).gameObject);
        }

        public virtual T Add(T prefabOverride = null)
        {
            var prefab = prefabOverride ? prefabOverride : this.prefab;

            var inst = Instantiate(prefab);
            inst.transform.SetParent(this.transform);
            inst.transform.localScale = Vector3.one;
            inst.transform.localRotation = Quaternion.identity;

            inst.gameObject.SetActive(true);

            return inst;
        }

        public virtual void Remove(T inst)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject == inst.gameObject)
                    DestroyImmediate(inst.gameObject);
            }
        }

        public virtual T Get(int index)
        {
            if (index >= 0 && index  < transform.childCount)
                return transform.GetChild(index).GetComponent<T>();

            return null;
        }

        public virtual void Remove(int index)
        {
            if (transform.childCount - 1 > index) return;
            if (transform.childCount <= 1) return;

            DestroyImmediate(transform.GetChild(index).gameObject);
        }

        public virtual void SetCount(int count)
        {
            if (!m_initialized) Awake();

            if (!gameObject.activeInHierarchy)
            {
                while (transform.childCount > count + 1)
                    DestroyImmediate(transform.GetChild(count).gameObject);
                while (transform.childCount < count + 1)
                    Add();

                return;
            }

            StopAllCoroutines();
            StartCoroutine(SetCountInternal(count));
        }

        protected IEnumerator SetCountInternal(int count)
        {
            while (transform.childCount > count + 1)
            {
                DestroyImmediate(transform.GetChild(count).gameObject);
                yield return new WaitForEndOfFrame();
            }

            while (transform.childCount < count + 1)
            {
                Add();
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
