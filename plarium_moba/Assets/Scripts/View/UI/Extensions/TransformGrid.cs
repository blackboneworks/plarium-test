﻿using System.Collections;
using UnityEngine;
using _UI.UIExtensions;

public class TransformGrid : GridController<Transform>
{
    public override void SetCount(int count)
    {
        StopAllCoroutines();
        StartCoroutine(SetCountInternal(count));
    }

    private IEnumerator SetCountInternal(int count)
    {
        while (transform.childCount > count + 1)
        {
            DestroyImmediate(transform.GetChild(count).gameObject);
            yield return new WaitForEndOfFrame();
        }

        while (transform.childCount < count + 1)
        {
            Add();
            yield return new WaitForEndOfFrame();
        }
    }
}
