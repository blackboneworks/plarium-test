﻿using Controllers.Game.Buldings;
using Controllers.Game.Units;
using UnityEngine;
using UnityEngine.UI;

namespace View.UI.Panels
{
    public class UIStatsPanel : MonoBehaviour
    {
        [SerializeField] private Text _unitName;
        [SerializeField] private Text _unitStats;
        [SerializeField] private Button _actionButton;

        private void Start()
        {
            Clean();
        }

        public void Clean()
        {
            _unitName.text = "";
            _unitStats.text = "";
            _actionButton.gameObject.SetActive(false);
            _actionButton.onClick.RemoveAllListeners();
        }

        public void Initialize(UnitController unit)
        {
            Clean();

            _unitName.text = unit.Config.name;

            _unitStats.text = "";
            foreach (var parameter in unit.GetParameters())
                _unitStats.text += string.Format("{0} : {1}\n\r", parameter.Key.Name,
                    parameter.Value.ToString("###.###"));
        }

        public void Initialize(SofaController sofa)
        {
            Clean();

            _unitName.text = sofa.Config.name;
            _unitStats.text = "Defend This Thing!";
        }

        public void Initialize(Barrack barrack)
        {
            Clean();

            _unitName.text = barrack.Config.name;
            _unitStats.text = "Level : " + barrack.Level + "\n\r";

            if (barrack.NextLevelPrice <= 0)
            {
                _actionButton.gameObject.SetActive(false);
                return;
            }

            _unitStats.text += "Next level price : " + barrack.NextLevelPrice + "\n\r";
            _actionButton.gameObject.SetActive(true);
            _actionButton.GetComponentInChildren<Text>().text = "Level Up";
            _actionButton.onClick.AddListener(() =>
            {
                if(barrack.LevelUp())
                    Initialize(barrack);
            });
        }
    }
}