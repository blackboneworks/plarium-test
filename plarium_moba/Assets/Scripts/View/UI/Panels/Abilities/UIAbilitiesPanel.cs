﻿using Controllers.Game.Units;
using Extensions;
using UnityEngine;

namespace View.UI.Panels
{
    public class UIAbilitiesPanel : MonoBehaviour
    {
        [SerializeField] private UIAbilityGrid _grid;

        public void Initialize(UnitController unit)
        {
            if(unit.AbilitiesHub.AbilitiesCount <= 1) gameObject.SetActive(false);

            gameObject.SetActive(true);

            _grid.Clear();
            for (int i = 1; i < unit.AbilitiesHub.AbilitiesCount; i++)
                _grid.Add().Initialize(unit.AbilitiesHub.GetAbility(i), i);
        }

        public void OnAbilityClick(int abilityIndex)
        {
            EventManager.Input.AlphaKey.Invoke(abilityIndex);
        }

        public void Clean()
        {
            _grid.Clear();
        }
    }
}
