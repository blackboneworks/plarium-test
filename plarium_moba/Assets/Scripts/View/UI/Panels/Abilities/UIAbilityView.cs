﻿using Models.Units.Abilities;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace View.UI.Panels
{
    public class UIAbilityView : UIBehaviour
    {
        [SerializeField] private UIAbilitiesPanel _abilitiesPanel;
        [SerializeField] private Text _level;
        [SerializeField] private Text _cooldownState;
        [SerializeField] private Image _icon;
        [SerializeField] private Image _cooldownOverlay;

        private UnitAbility _ability;
        private int _abilityIndex;

        public void Initialize(UnitAbility ability, int abilityIndex)
        {
            _ability = ability;

            if (_ability is ILeveledAbility)
            {
                _level.text = (_ability as ILeveledAbility).Level.ToString();
                (_ability as ILeveledAbility).AbilityLeveledUp += OnAbilityLevelUp;
            }
            else
                _level.text = "";

            _icon.sprite = _ability.Icon;
            _cooldownOverlay.sprite = _ability.Icon;

            _cooldownState.text = "";
            _cooldownOverlay.fillAmount = 0f;
        }

        private void OnAbilityLevelUp(int level)
        {
            _level.text = (_ability as ILeveledAbility).Level.ToString();
        }

        public void OnPress()
        {
            _abilitiesPanel.OnAbilityClick(_abilityIndex);
        }

        protected override void OnDestroy()
        {
            (_ability as ILeveledAbility).AbilityLeveledUp -= OnAbilityLevelUp;
        }

        public void FixedUpdate()
        {
            if (!_ability is ICooldownAbility) return;

            var cdAbility = _ability as ICooldownAbility;
            var cd = _ability.LastUsedTime + cdAbility.Cooldown - Time.time;
            if (cd > 0) _cooldownState.text = cd.ToString("###0.0s");
            else _cooldownState.text = "";
            _cooldownOverlay.fillAmount = Mathf.Clamp01(cd / cdAbility.Cooldown);
        }
    }
}