﻿using UnityEngine;
using Controllers.Selection;

public class PointerControllerView : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private PointerController _controller;

    [Header("Links")]
    [SerializeField] private RectTransform _frameRectTransform;

    private void Update()
    {
        _frameRectTransform.gameObject.SetActive(_controller.SelectionRect != null);
        if(_controller.SelectionRect == null) return;

        var rect = _controller.SelectionRect.Value;
        _frameRectTransform.anchoredPosition = rect.position;
        _frameRectTransform.sizeDelta = rect.size;
    }
}
