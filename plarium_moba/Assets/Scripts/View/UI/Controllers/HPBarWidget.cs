﻿using Controllers.Game.Units.HealthSystem;
using UnityEngine;

namespace View.Units
{
    [ExecuteInEditMode]
    public class HPBarWidget : MonoBehaviour
    {
        [SerializeField] private bool _billboard = true;
        [SerializeField] private Transform _scaler;

        public float Value
        {
            get { return _scaler.localScale.x; }
            private set
            {
                var s = _scaler.localScale;
                s.x = value;
                _scaler.localScale = s;
            }
        }
        public HealthHub Hub { get; set; }

        void Update()
        {
            // billboard
            if(_billboard)
                transform.forward = -Camera.main.transform.forward;

            if(Hub == null) return;
            Value = Hub.NormalizedHealth;
        }
    }
}
