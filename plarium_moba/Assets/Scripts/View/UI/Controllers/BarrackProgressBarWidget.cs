﻿using Controllers.Game.Buldings;
using UnityEngine;

namespace View.UI.Controllers
{
    public class BarrackProgressBarWidget : MonoBehaviour
    {

        [SerializeField] private bool _billboard = true;
        [SerializeField] private Transform _scaler;
        [SerializeField] private Barrack _barrack;

        public float Value
        {
            get { return _scaler.localScale.x; }
            private set
            {
                var s = _scaler.localScale;
                s.x = value;
                _scaler.localScale = s;
            }
        }

        private void Update()
        {
            // billboard
            if (_billboard)
                transform.forward = -Camera.main.transform.forward;

            if (_barrack == null) return;

            Value = _barrack.CurrentUnitProgress;
        }
    }
}
