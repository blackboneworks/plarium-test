﻿using System.Collections.Generic;
using Controllers.Cameras;
using Controllers.Game.Buldings;
using Controllers.Game.Units;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using View.UI.Panels;
using Selectable = Controllers.Selection.Selectable;

namespace View.UI
{
    public class UIController : MonoBehaviour
    {
        [Header("Panels")]
        [SerializeField] private UIStatsPanel _statsPanel;
        [SerializeField] private UIAbilitiesPanel _abilitiesPanel;

        [Header("Parts")]
        [SerializeField] private CameraController _camera;
        [SerializeField] private Toggle _camSwitchToggle;
        [SerializeField] private Text _heroRespawnTimerLabel;
        [SerializeField] private GameObject _winPanel;
        [SerializeField] private GameObject _loosePanel;

        private void Awake()
        {
            EventManager.Input.SigleSelected.OnEvent += OnSingleSelected;
            EventManager.Input.GroupSelected.OnEvent += OnGroupSelected;
            EventManager.Hero.HeroRespawnCountdownStarted.OnEvent += OnHeroWillBeSpawned;
            EventManager.Game.GameEnded.OnEvent += OnGameEnded;

            _camSwitchToggle.onValueChanged.AddListener(ToggleChanged);
        }

        private void OnDestroy()
        {
            EventManager.Input.SigleSelected.OnEvent -= OnSingleSelected;
            EventManager.Input.GroupSelected.OnEvent -= OnGroupSelected;
            EventManager.Hero.HeroRespawnCountdownStarted.OnEvent -= OnHeroWillBeSpawned;
            EventManager.Hero.HeroRespawnCountdownStarted.OnEvent -= OnHeroWillBeSpawned;

            _camSwitchToggle.onValueChanged.RemoveAllListeners();
        }

        private void OnGameEnded(bool win)
        {
            _winPanel.SetActive(win);
            _loosePanel.SetActive(!win);
        }

        public void ContinueAfterGameEnd()
        {
            // reboot
            Application.LoadLevel(Application.loadedLevel);
        }

        private void OnHeroWillBeSpawned(float delay)
        {
            for (int i = 0; i < delay; i++)
            {
                var time = i;
                this.Invoke(() => _heroRespawnTimerLabel.text = "Hero respawn in " + time + "s.", delay - i);
            }
            this.Invoke(() => _heroRespawnTimerLabel.text = "", delay);
        }

        private void ToggleChanged(bool toggled)
        {
            _camSwitchToggle.onValueChanged.RemoveListener(ToggleChanged);
            _camera.SetFollowMode(toggled);
            _camSwitchToggle.isOn = _camera.FollowMode;
            _camSwitchToggle.onValueChanged.AddListener(ToggleChanged);
        }

        private void OnGroupSelected(IEnumerable<Selectable> obj)
        {
            _statsPanel.Clean();
            _abilitiesPanel.Clean();
        }

        private void OnSingleSelected(Selectable selection)
        {
            if (selection.Handler is UnitController)
            {
                var unit = selection.Handler as UnitController;
                _statsPanel.Initialize(unit);

                if (unit.IsHero)
                    _abilitiesPanel.Initialize(unit);
            }

            if (selection.Handler is SofaController)
            {
                var sofa = selection.Handler as SofaController;
                _statsPanel.Initialize(sofa);
            }

            if (selection.Handler is Barrack)
            {
                var barrack = selection.Handler as Barrack;
                _statsPanel.Initialize(barrack);
            }
        }
    }
}