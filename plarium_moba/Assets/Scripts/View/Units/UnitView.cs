﻿using Controllers.Game.Units;
using UnityEngine;
using View.Units;

public class UnitView : MonoBehaviour, IUnitView
{
    [Header("References")]
    [SerializeField] private Animator _animator;
    [SerializeField] private HPBarWidget _hpBar;

    [Header("Selection")]
    [SerializeField] private GameObject _selectionView;
    [Header("Attackable")]
    [SerializeField] private GameObject _targetHighlight;

    private UnitController _unit;

    public void Initialize(UnitController unit)
    {
        _unit = unit;

        _selectionView.SetActive(false);
        _targetHighlight.SetActive(false);

        _hpBar.Hub = _unit.HealthHub;
    }
    
    public void SetMoving(bool moving)
    {
        _animator.SetBool("IsMoving", moving);
    }
    
    public void OnSelected()
    {
        _selectionView.SetActive(true);
    }

    public void OnDeselected()
    {
        _selectionView.SetActive(false);
    }

    public void AbilityUsed(int abilityIndex)
    {
        if(_animator == null) return;

        _animator.SetInteger("AbilityIndex", abilityIndex);
        _animator.SetTrigger("UseAbility");
    }

    public void TargetHighlight(bool enabled)
    {
        _targetHighlight.gameObject.SetActive(enabled);
    }
}